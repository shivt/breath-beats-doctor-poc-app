import grails.plugin.springsecurity.SecurityConfigType

s3.server.access.key.id = 'AKIAJCWDP3ZOZDUASXRQ'
s3.server.access.secret.key = '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'
s3.server.access.bucketName = 'revoluno'
s3.server.access.profile.image.loc = 'doc-profile'
s3.server.access.consultation.loc = 'doctor-consultations'
msg.sms.smsgupshup.url = 'http://enterprise.smsgupshup.com'
msg.sms.smsgupshup.gateway.rest = 'GatewayAPI/rest'
msg.sms.smsgupshup.gateway.sendmethod = 'SendMessage'
msg.sms.smsgupshup.gateway.userid = '2000167588'
msg.sms.smsgupshup.gateway.password = '12345'
msg.sms.smsgupshup.gateway.otp.length = '6'
consultation.fee.timePeriod.default = 30
profile.image.max.alowed.size=512000 // IN Byte
otp.expiration.interval = 60 // in seconds
consultation.delay.notification.timer = 30 // in seconds
firebase.message.key="AIzaSyC6F8DOR-sWwQsIbtxdvBerKKiV8uWsuf4"
firebase.message.apiUrl="https://fcm.googleapis.com/fcm/send"

// locations to search for config files that get merged into the main config;
// config files can be ConfigSlurper scripts, Java properties files, or classes
// in the classpath in ConfigSlurper format

// grails.config.locations = [ "classpath:${appName}-config.properties",
//                             "classpath:${appName}-config.groovy",
//                             "file:${userHome}/.grails/${appName}-config.properties",
//                             "file:${userHome}/.grails/${appName}-config.groovy"]

// if (System.properties["${appName}.config.location"]) {
//    grails.config.locations << "file:" + System.properties["${appName}.config.location"]
// }

grails.project.groupId = appName // change this to alter the default package name and Maven publishing destination

// The ACCEPT header will not be used for content negotiation for user agents containing the following strings (defaults to the 4 major rendering engines)
grails.mime.disable.accept.header.userAgents = ['Gecko', 'WebKit', 'Presto', 'Trident']
grails.mime.types = [ // the first one is the default format
                      all          : '*/*', // 'all' maps to '*' or the first available format in withFormat
                      atom         : 'application/atom+xml',
                      css          : 'text/css',
                      csv          : 'text/csv',
                      form         : 'application/x-www-form-urlencoded',
                      html         : ['text/html', 'application/xhtml+xml'],
                      js           : 'text/javascript',
                      json         : ['application/json', 'text/json'],
                      multipartForm: 'multipart/form-data',
                      rss          : 'application/rss+xml',
                      text         : 'text/plain',
                      hal          : ['application/hal+json', 'application/hal+xml'],
                      xml          : ['text/xml', 'application/xml'],
                      jpg          : 'image/jpeg',
                      png          : 'image/png',
                      gif          : 'image/gif',
                      bmp          : 'image/bmp'
]

// URL Mapping Cache Max Size, defaults to 5000
//grails.urlmapping.cache.maxsize = 1000

// Legacy setting for codec used to encode data with ${}
grails.views.default.codec = "html"

// The default scope for controllers. May be prototype, session or singleton.
// If unspecified, controllers are prototype scoped.
grails.controllers.defaultScope = 'singleton'

// GSP settings
grails {
    views {
        gsp {
            encoding = 'UTF-8'
            htmlcodec = 'xml' // use xml escaping instead of HTML4 escaping
            codecs {
                expression = 'html' // escapes values inside ${}
                scriptlet = 'html' // escapes output from scriptlets in GSPs
                taglib = 'none' // escapes output from taglibs
                staticparts = 'none' // escapes output from static template parts
            }
        }
        // escapes all not-encoded output at final stage of outputting
        // filteringCodecForContentType.'text/html' = 'html'
    }
}


grails.converters.encoding = "UTF-8"
// scaffolding templates configuration
grails.scaffolding.templates.domainSuffix = 'Instance'

// Set to false to use the new Grails 1.2 JSONBuilder in the render method
grails.json.legacy.builder = false
// enabled native2ascii conversion of i18n properties files
grails.enable.native2ascii = true
// packages to include in Spring bean scanning
grails.spring.bean.packages = []
// whether to disable processing of multi part requests
grails.web.disable.multipart = false

// request parameters to mask when logging exceptions
grails.exceptionresolver.params.exclude = ['password']

// configure auto-caching of queries by default (if false you can cache individual queries with 'cache: true')
grails.hibernate.cache.queries = false

// configure passing transaction's read-only attribute to Hibernate session, queries and criterias
// set "singleSession = false" OSIV mode in hibernate configuration after enabling
grails.hibernate.pass.readonly = false
// configure passing read-only to OSIV session by default, requires "singleSession = false" OSIV mode
grails.hibernate.osiv.readonly = false

environments {
    development {
        grails.logging.jul.usebridge = true
    }
    production {
        grails.logging.jul.usebridge = false
        // TODO: grails.serverURL = "http://www.changeme.com"
    }
}

// log4j configuration
String logAppName = 'BreathBeatsAPI'
String loggingFileDir = "target/logs"
String loggingFileName = "${loggingFileDir}/${logAppName}.log".toString()
log4j.main = {
    // Example of changing the log pattern for the default console appender:
    //
    appenders {
        rollingFile name: 'appLog', file: loggingFileName, maxFileSize: "${10 * 1024 * 1024}", maxBackupIndex: 10, layout: pattern( conversionPattern: '%d{[EEE, dd-MMM-yyyy @ HH:mm:ss.SSS]} [%t] %-5p %c %x - %m%n' )
    }
    root {
        error 'stdout', 'appLog'
        additivity = true
    }
    error 'org.codehaus.groovy.grails.web.servlet',        // controllers
          'org.codehaus.groovy.grails.web.pages',          // GSP
          'org.codehaus.groovy.grails.web.sitemesh',       // layouts
          'org.codehaus.groovy.grails.web.mapping.filter', // URL mapping
          'org.codehaus.groovy.grails.web.mapping',        // URL mapping
          'org.codehaus.groovy.grails.commons',            // core / classloading
          'org.codehaus.groovy.grails.plugins',            // plugins
          'org.codehaus.groovy.grails.orm.hibernate',      // hibernate integration
          'org.springframework'
    debug 'breath.beats.doctor.poc.app'
    debug 'com.breath.beats.doctor.app.common'
    debug 'breathbeatsapi'

    error 'org.hibernate',
          'net.sf.ehcache.hibernate'
}

grails.plugin.springsecurity.securityConfigType = 'InterceptUrlMap'
grails.plugin.springsecurity.securityConfigType = SecurityConfigType.InterceptUrlMap
grails.plugin.springsecurity.interceptUrlMap = [
        "/login/auth": ["permitAll"],
        "/logout/**" : ["permitAll"],
        "/**"        : ["IS_AUTHENTICATED_ANONYMOUSLY"],
        '/'          : ['IS_AUTHENTICATED_FULLY']
]
// Added by the Spring Security Core plugin:
useRestApiAuthenticationEntryPoint = true
grails.plugin.springsecurity.userLookup.userDomainClassName = 'login.SecUser'
grails.plugin.springsecurity.userLookup.authorityJoinClassName = 'login.SecUserSecRole'
grails.plugin.springsecurity.authority.className = 'login.SecRole'
grails.plugin.springsecurity.controllerAnnotations.staticRules = [
        '/'              : ['permitAll'],
        '/index'         : ['permitAll'],
        '/index.gsp'     : ['permitAll'],
        '/assets/**'     : ['permitAll'],
        '/**/js/**'      : ['permitAll'],
        '/**/css/**'     : ['permitAll'],
        '/**/images/**'  : ['permitAll'],
        '/**/favicon.ico': ['permitAll']
]

