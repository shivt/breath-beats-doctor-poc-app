class SetCurrentUserFilters {
    def springSecurityService
    def filters = {
        all( controller: 'person', action: '*' ) {
            before = {
                println 'Check if user logger in '
                if (springSecurityService.isLoggedIn()) {
                    println 'Logged in  '
                    request.setAttribute( "current_user", springSecurityService.currentUser );
                } else {
                    println 'Not Logged in  '
                    redirect( controller: "login", action: 'ajaxDenied' )
                }
            }
            after = {Map model ->

            }
            afterView = {Exception e ->

            }
        }
        login( controller: '*', action: '*' ) {
            before = {
                if (springSecurityService.isLoggedIn()) {
                    println 'Set logged in user details  ' + springSecurityService.currentUser
                    request.setAttribute( "current_user", springSecurityService.currentUser );
                }
            }
            after = {Map model ->

            }
            afterView = {Exception e ->

            }
        }
    }
}
