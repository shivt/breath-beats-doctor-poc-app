package login

class SecUser implements Serializable {

    private static final long serialVersionUID = 1

    transient springSecurityService
    String id
    String username
    String password
    String firstName
    String lastName
    boolean enabled = true
    boolean accountExpired
    boolean accountLocked
    boolean passwordExpired
    Long version


    SecUser( String username, String password ) {
        this()
        this.username = username
        this.password = password
    }


    @Override
    int hashCode() {
        username?.hashCode() ?: 0
    }


    @Override
    boolean equals( other ) {
        is( other ) || (other instanceof SecUser && other.username == username)
    }


    @Override
    String toString() {
        username
    }


    Set<SecRole> getAuthorities() {
        SecUserSecRole.findAllBySecUser( this )*.secRole
    }


    def beforeInsert() {
        encodePassword()
    }


    def beforeUpdate() {
        if (isDirty( 'password' )) {
            encodePassword()
        }
    }


    protected void encodePassword() {
        password = springSecurityService?.passwordEncoder ? springSecurityService.encodePassword( password ) : password
    }

    static transients = ['springSecurityService']

    static constraints = {
        username blank: false, unique: true
        password blank: false
    }

    static mapping = {
        id column: "USER_ID", generator: "guid"
        table 'BB_SEC_USER'
        password column: '`password`'
        firstName column: 'first_name'
        lastName column: 'last_name'
        enabled column: "ENABLED"
        accountExpired column: "ACCOUNT_EXPIRED"
        accountLocked column: "ACCOUNT_LOCKED"
        passwordExpired column: 'PASSWORD_EXPIRED'
    }
}
