package login

class SecRole implements Serializable {

    private static final long serialVersionUID = 1
    String id
    String authority

    SecRole( String authority ) {
        this()
        this.authority = authority
    }


    @Override
    int hashCode() {
        authority?.hashCode() ?: 0
    }


    @Override
    boolean equals( other ) {
        is( other ) || (other instanceof SecRole && other.authority == authority)
    }


    @Override
    String toString() {
        authority
    }

    static constraints = {
        authority blank: false, unique: true
    }

    static mapping = {
        table 'BB_SEC_ROLE'
        cache true
        authority column: 'authority'
        id column: "ROLE_ID", generator: "guid"
    }
}
