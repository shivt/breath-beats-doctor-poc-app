package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Doctor Details .. used for search
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class DoctorDetails {
    String uuid
    String id
    String doctorId
    String qualification
    String specialityName
    String addressLine1
    String firstName
    String lastName
    String emailAddress
    String primaryContact
    String city
    String zipCode
    String addressLine2
    String doctorImageLocUrl
    int expYear
    int feeForPatient
    int timeDurationForPatient
    static mapping = {
        table( 'bb_v_doctor_details' )
        id column: 'person_id', insert: false, update: false
        uuid column: "hospital_number"
        specialityName column: "speciality_name"
        addressLine1 column: "address_line1"
        firstName column: "first_name"
        lastName column: "last_name"
        emailAddress column: "email_Address"
        feeForPatient column: 'fee_for_patient'
        timeDurationForPatient column: 'time_duration_for_patient'
        primaryContact column: "primary_contact"
        city column: "city"
        zipCode column: "zip_code"
        doctorId column: "doctor_id"
        addressLine1 column: "address_line1"
        expYear column: 'exp_year'
        addressLine2 column: "address_line2"
        qualification column: "qualification"
        doctorImageLocUrl column: 'doctor_image'
        version false
    }
}
