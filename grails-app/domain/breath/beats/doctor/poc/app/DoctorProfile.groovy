package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Doctor Profile
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class DoctorProfile {
    String id
    Doctor doctor
    Integer feeForPatient
    Integer feeForDiscussion
    Integer timeDurationForPatient
    Integer timeDurationForDiscussion
    String doctorImage
    Status status
    Status currentStatus
    Integer expYear
    String hospitalWorkedIn
    String certifications
    String degreesHeld
    String awards
    String homeCareAvailability
    String clinicContactNumber
    String clinicAddress
    String createdBy
    Date createdOn
    Long version

    static mapping = {
        table( 'bb_doctor_profile' )
        id column: "doctor_profile_id", generator: "guid"
        doctor column: "doctor_id"
        feeForPatient column: "fee_for_patient"
        feeForDiscussion column: "fee_for_discussion"
        timeDurationForPatient column: "time_duration_for_patient"
        timeDurationForDiscussion column: "time_duration_for_discussion"
        doctorImage column: 'doctor_image'
        status column: "status_id"
        currentStatus column: 'currentStatus_id'
        expYear column: 'exp_year'
        hospitalWorkedIn column: 'hospital_worked_in'
        certifications column: 'certifications'
        degreesHeld column: 'degrees_held'
        awards column: 'awards'
        homeCareAvailability column: 'home_care_availability'
        clinicContactNumber column: 'clinic_contant_number'
        clinicAddress column: 'clinic_address'
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"

    }
    static constraints = {
        doctor nullable: false
        feeForPatient nullable: true
        feeForDiscussion nullable: true
        timeDurationForPatient nullable: true
        timeDurationForDiscussion nullable: true
        doctorImage nullable: true
        status nullable: false
        currentStatus nullable: true
        createdBy nullable: false
        createdOn nullable: false
        expYear nullable: true
        hospitalWorkedIn nullable: true
        certifications nullable: true
        degreesHeld nullable: true
        awards nullable: true
        homeCareAvailability nullable: true
        clinicContactNumber nullable: true
        clinicAddress nullable: true

    }
}
