package breath.beats.doctor.poc.app

/**
 * Domain class to define the Roles
 * Roles can be patient, doctor, staff, admin , visitor, etc
 */
class Role {
    String id
    String roleKey
    String roleName
    String roleDescription
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_role' )
        id column: "role_id", generator: "guid"
        roleKey column: "role_key"
        roleName column: "role_name"
        roleDescription column: "role_description"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        roleKey nullable: false, maxSize: 256
        roleName nullable: false, maxSize: 256
        roleDescription nullable: true, maxSize: 256
        createdBy nullable: false
        createdOn nullable: false
    }
}
