package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Consultation Log
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class DoctorPatientConsultationLog {
    String id
    DoctorPatientConsultation doctorPatientConsultation
    ConsultationStage consultationStage
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_patient_consultation_log' )
        id column: 'consultation_log_id', generator: "guid"
        doctorPatientConsultation column: "consultation_id"
        consultationStage column: "consultation_stages_id"
        createdBy column: 'created_by'
        createdOn column: 'created_on'

    }
    static constraints = {
        doctorPatientConsultation nullable: false
        consultationStage nullable: false
        createdBy nullable: false
        createdOn nullable: false
    }
}
