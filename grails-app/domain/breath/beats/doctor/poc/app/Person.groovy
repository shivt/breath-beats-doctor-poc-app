package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Person
 * Person can be anyone who is part of Hospital even a patient
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class Person {
    String id
    String uuid
    String firstName
    String middleName
    String lastName
    String gender
    Date dateOfBirth
    String emailAddress
    Integer age
    String primaryContact
    String secondaryContact
    String maritalStatus
    String addressLine1
    String addressLine2
    String addressLine3
    String city
    String state
    String country
    String zipCode
    Role role
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_person' )
        id column: "person_id", generator: "guid"
        uuid column: "hospital_number"
        firstName column: "first_name"
        middleName column: "middle_name"
        lastName column: "last_name"
        gender column: "gender"
        dateOfBirth column: "date_of_birth"
        emailAddress column: 'email_Address'
        age column: "age"
        primaryContact column: "primary_contact"
        secondaryContact column: "secondary_contact"
        maritalStatus column: "marital_status"
        addressLine1 column: "address_line1"
        addressLine2 column: "address_line2"
        addressLine3 column: "address_line3"
        city column: "city"
        state column: "state_name"
        role column: "role_id"
        country column: "country"
        zipCode column: "zip_code"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        uuid nullable: false
        firstName nullable: false
        middleName nullable: true
        lastName nullable: false
        gender nullable: true
        dateOfBirth nullable: true
        emailAddress nullable: true
        age nullable: true
        primaryContact nullable: false
        secondaryContact nullable: true
        maritalStatus nullable: true
        addressLine1 nullable: true
        addressLine2 nullable: true
        addressLine3 nullable: true
        city nullable: true
        state nullable: true
        role nullable: false
        country nullable: true
        zipCode nullable: true
        createdBy nullable: false
        createdOn nullable: false
    }
}
