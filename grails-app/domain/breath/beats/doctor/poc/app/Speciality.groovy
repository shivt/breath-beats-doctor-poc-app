package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Specialty
 * Specialty can be General Pediatrician etc
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class Speciality {
    String id
    String name
    String description
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_speciality' )
        id column: "speciality_id", generator: "guid"
        name column: "speciality_name"
        description column: "speciality_description"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        name nullable: false, maxSize:  256
        description nullable: true, maxSize: 300
        createdBy nullable: false
        createdOn nullable: false
    }
}
