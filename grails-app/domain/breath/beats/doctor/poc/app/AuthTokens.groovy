package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Auth Tokens
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class AuthTokens {
    String key
    String id
    String token
    String tokenType
    Date generatedOn
    String status
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_auth_token' )
        id column: 'bb_auth_token_id ', generator: 'guid'
        key column: "key_id"
        token column: "token"
        tokenType column: "token_type"
        generatedOn column: "generated_on"
        status column: "status"
        createdBy column: "created_by"
        createdOn column: "last_modified_on"
        version column: "version"
    }
    static constraints = {
        key nullable: false
        token nullable: false
        status nullable: false
        generatedOn nullable: false
        tokenType nullable: true
        createdBy nullable: false
        createdOn nullable: false
        version nullable: true
    }
}
