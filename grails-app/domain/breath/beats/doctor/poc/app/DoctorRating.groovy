package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Doctor Rating
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class DoctorRating {
    String id
    Doctor doctor
    DoctorPatientConsultation doctorPatientConsultation
    Integer rating
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_doctor_rating' )
        id column: 'doctor_rating_id', generator: "guid"
        doctor column: "doctor_id"
        doctorPatientConsultation column: "consultation_id"
        rating column: "rating"
        createdBy column: 'created_by'
        createdOn column: 'created_on'

    }
    static constraints = {
        doctor nullable: false
        doctorPatientConsultation nullable: false
        rating nullable: true
        createdBy nullable: false
        createdOn nullable: false
    }
}
