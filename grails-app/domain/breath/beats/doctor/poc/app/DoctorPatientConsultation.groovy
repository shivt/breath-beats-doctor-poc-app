package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Doctor Patient Consultation
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class DoctorPatientConsultation {
    String id
    Doctor doctor
    Patient patient
    ConsultationMode consultationMode
    ConsultationStage consultationStage
    Date consStartDate
    Date consEndDate
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_patient_consultation' )
        id column: 'consultation_id', generator: "guid"
        doctor column: "doctor_id"
        patient column: 'patient_id'
        consultationMode column: "consultation_mode_id"
        consStartDate column: "cons_start_date"
        consEndDate column: "cons_end_date"
        consultationStage column: "consultation_stages_id"
        createdBy column: 'created_by'
        createdOn column: 'created_on'

    }
    static constraints = {
        doctor nullable: true
        patient nullable: true
        doctor nullable: false
        consultationStage nullable: false
        consultationMode nullable: true
        consStartDate nullable: false
        consEndDate nullable: true
        createdBy nullable: false
        createdOn nullable: false
    }
}
