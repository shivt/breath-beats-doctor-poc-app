package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Consultation Stage
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class ConsultationStage {
    String name
    String id
    String description
    static mapping = {
        table( 'bb_consultation_stages' )
        id column: 'consultation_stages_id', insert: false, update: false
        name column: "name"
        description column: "description"
        version false
    }
}
