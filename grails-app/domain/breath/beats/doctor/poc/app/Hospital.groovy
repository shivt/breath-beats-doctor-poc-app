package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Hospital
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class Hospital {
    String id
    String hospitalLocation
    String hospitalName
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_hospital' )
        id column: "hospital_id", generator: "guid"
        hospitalName column: "hospital_name"
        hospitalLocation column: "hospital_location"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        hospitalName nullable: false, maxSize: 256
        hospitalLocation nullable: false
        createdBy nullable: false
        createdOn nullable: false
    }
}
