package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Consultation Mode
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class ConsultationMode {
    String name
    String id
    String description
    static mapping = {
        table( 'bb_consultation_mode' )
        id column: 'consultation_mode_id', insert: false, update: false
        name column: "name"
        description column: "description"
        version false
    }
}
