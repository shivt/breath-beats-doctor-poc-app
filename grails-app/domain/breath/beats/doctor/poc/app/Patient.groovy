package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Patient
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class Patient {
    String id
    String name
    String contactNumber
    String emailAddress
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_patient' )
        id column: "patient_id", generator: "guid"
        name column: "name"
        contactNumber column: "concat_number"
        emailAddress column: "email_Address"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        name nullable: false
        contactNumber nullable: false
        emailAddress nullable: true
        createdBy nullable: false
        createdOn nullable: false
    }
}
