package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Status
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class Status {
    String id
    String name
    String description
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_status' )
        id column: "status_id", generator: "guid"
        name column: "status_name"
        description column: "status_desc"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        name nullable: false, maxSize: 256
        description nullable: true, maxSize: 300
        createdBy nullable: false
        createdOn nullable: false
    }
}
