package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Doctor Popularity Details
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class DoctorPopularity {
    String id
    Doctor doctor
    Integer likes
    Integer dislike
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_doctor_pop' )
        id column: 'doctor_pop_id', generator: "guid"
        doctor column: "doctor_id"
        likes column: "likes"
        dislike column: "dislike"
        createdBy column: 'created_by'
        createdOn column: 'created_on'

    }
    static constraints = {
        likes nullable: true
        dislike nullable: true
        doctor nullable: false
        createdBy nullable: false
        createdOn nullable: false
    }
}
