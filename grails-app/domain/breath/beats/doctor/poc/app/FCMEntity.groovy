package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Firebase messaging
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class FCMEntity {
    String id
    String devicePhoneNumber
    String fcmNumber
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_fcm_entity' )
        id column: "fcm_entity_id", generator: "guid"
        devicePhoneNumber column: "device_phone_number"
        fcmNumber column: "fcm_number"
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        devicePhoneNumber nullable: false
        fcmNumber nullable: false
        createdBy nullable: false
        createdOn nullable: false
    }
}
