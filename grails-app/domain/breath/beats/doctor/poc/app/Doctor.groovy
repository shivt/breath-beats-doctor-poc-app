package breath.beats.doctor.poc.app

import groovy.transform.EqualsAndHashCode
import groovy.transform.ToString

/**
 * Domain class to define the Doctor
 */
@ToString(includeNames = true, ignoreNulls = true)
@EqualsAndHashCode(includeFields = true)
class Doctor {
    String id
    String uuid
    Speciality speciality
    String qualification
    String certificateNumber
    String certificateImgLoc
    String createdBy
    Date createdOn
    Long version
    static mapping = {
        table( 'bb_doctor' )
        id column: "doctor_id", generator: "guid"
        uuid column: "hospital_number"
        speciality column: "speciality_id"
        qualification column: "qualification"
        certificateNumber column: 'certificate_number'
        certificateImgLoc column: 'certificate_img_loc'
        createdBy column: "created_by"
        createdOn column: "created_on"
        version column: "version"
    }
    static constraints = {
        uuid nullable: false
        speciality nullable: true
        qualification nullable: true
        certificateNumber nullable: false
        certificateImgLoc nullable: true
        createdBy nullable: false
        createdOn nullable: false
    }
}
