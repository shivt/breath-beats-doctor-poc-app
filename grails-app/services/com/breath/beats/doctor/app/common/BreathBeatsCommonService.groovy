package com.breath.beats.doctor.app.common

import breath.beats.doctor.poc.app.FCMEntity
import breath.beats.doctor.poc.app.Speciality
import com.amazonaws.auth.AWSCredentials
import com.amazonaws.auth.BasicAWSCredentials
import com.amazonaws.services.s3.AmazonS3
import com.amazonaws.services.s3.AmazonS3Client
import com.amazonaws.services.s3.model.*
import com.amazonaws.util.json.JSONObject
import grails.util.Holders
import groovy.sql.Sql
import org.apache.http.HttpResponse
import org.apache.http.client.HttpClient
import org.apache.http.client.methods.HttpGet
import org.apache.http.client.methods.HttpPost
import org.apache.http.entity.StringEntity
import org.apache.http.impl.client.DefaultHttpClient
import org.apache.log4j.Logger
import org.springframework.web.multipart.MultipartFile

class BreathBeatsCommonService {
    def sessionFactory
    public static final String DR_NAME_PREFIX = 'Dr. '
    public static final String CREATED_BY = 'SYSTEM'
    public static final String SUFFIX = '/'
    private String accessKeyId = Holders.config.s3.server.access.key.id
    private String accessSecretKey = Holders.config.s3.server.access.secret.key
    private String accessBucketName = Holders.config.s3.server.access.bucketName
    Logger logger = Logger.getLogger( this.class.name )

    def getSequenceNumber( systemId ) {
        Sql sql = new Sql( sessionFactory.getCurrentSession().connection() )
        def generator = ''
        sql.call( "{call bb_gen_seq_number(?, ?)}", [systemId, Sql.VARCHAR] ) {
            out -> generator = out
        }
        generator
    }


    def downloadDocumentFromAWSS3( folderName, String fileName ) {

        def result = [success: true]
        if (!folderName) {
            result.message = 'folder name is required'
            result.success = false
            return result
        }
        if (!fileName) {
            result.message = 'file name is required'
            result.success = false
            return result
        }
        logger.debug( "$accessKeyId $accessSecretKey $accessBucketName" )
        try {
            AWSCredentials credentials = new BasicAWSCredentials( "$accessKeyId", "$accessSecretKey" );
            AmazonS3 s3client = new AmazonS3Client( credentials );
            if (!s3client.listBuckets().name?.contains( accessBucketName )) {
                result.message = 'Bucket used for download is not present'
                result.success = false
            }
            S3Object s3Object = s3client.getObject( new GetObjectRequest( accessBucketName, fileName ) );
            result.fileContent = s3Object.getObjectContent().bytes
            logger.debug( 's3Object.getObjectContent()' + s3Object.getObjectContent() )
        } catch (AmazonS3Exception ams3e) {
            result.message = 'AWS Error while uploading file. ' + ams3e.message
            result.success = false
        }
        catch (Throwable te) {
            result.message = 'Other Error while uploading file. ' + te.message
            result.success = false
        }
        return result
    }

    /**
     * Uploads Document To AWS S3
     * @param folderName
     * @param file
     * @return
     */
    def uploadDocumentToAWSS3( folderName, MultipartFile file ) {
        def result = [success: true]
        if (!folderName) {
            result.message = 'Folder Name is required'
            result.success = false
            return result
        }
        if (!file) {
            result.message = 'file is required'
            result.success = false
            return result
        }
        logger.debug( "$accessKeyId $accessSecretKey $accessBucketName " )
        try {
            AWSCredentials credentials = new BasicAWSCredentials( "$accessKeyId", "$accessSecretKey" );
            AmazonS3 s3client = new AmazonS3Client( credentials );
            if (!s3client.listBuckets()?.name.contains( accessBucketName )) {
                result.message = 'Bucket used for upload is not present'
                result.success = false
            }
            createFolder( accessBucketName, folderName, s3client )
            def fileName = folderName + SUFFIX + file.getOriginalFilename()
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentLength( file.size );
            PutObjectRequest putObjectRequest = new PutObjectRequest( accessBucketName, fileName, file.inputStream, metadata )
            PutObjectResult resultUpload = s3client.putObject( putObjectRequest )
            putObjectRequest.getKey()
            result.url = fileName
            logger.debug( "resultUpload" + resultUpload )
        } catch (AmazonS3Exception ams3e) {
            result.message = 'AWS Error while uploading file. ' + ams3e.message
            result.success = false
        }
        catch (Throwable te) {
            result.message = 'Other Error while uploading file. ' + te.message
            result.success = false
        }
        return result
    }

    /**
     * Creates Folder
     * @param bucketName
     * @param folderName
     * @param client
     */
    private void createFolder( String bucketName, String folderName, AmazonS3 client ) {
        // create meta-data for your folder and set content-length to 0
        ObjectMetadata metadata = new ObjectMetadata();
        metadata.setContentLength( 0 );
        // create empty content
        InputStream emptyContent = new ByteArrayInputStream( new byte[0] );
        // create a PutObjectRequest passing the folder name suffixed by /
        PutObjectRequest putObjectRequest = new PutObjectRequest( bucketName,
                                                                  folderName + '/', emptyContent, metadata );
        // send request to S3 to create folder
        PutObjectResult result = client.putObject( putObjectRequest );
        logger.debug( "result" + result )
    }

    /**
     *
     * @param filename
     * @return
     */
    static String getExtensionFromFilenameLowercase( filename ) {
        def returned_value = ""
        def m = (filename =~ /(\.[^\.]*)$/)
        if (m.size() > 0) returned_value = ((m[0][0].size() > 0) ? m[0][0].substring( 1 ).trim().toLowerCase() : "");
        return returned_value
    }

    /**
     * Validate Request for Sign Up
     * @param map
     * @return
     */
    def validateRequest( map ) {
        def retMsg = []
        if (!map.firstName) {
            retMsg << 'First Name missing'
        }
        if (!map.lastName) {
            retMsg << 'Last Name missing'
        }
        if (!map.primaryContact) {
            retMsg << 'Contact Number missing'
        }
        if (!map.emailAddress) {
            retMsg << 'Email Address missing'
        }
        if (!map.speciality) {
            retMsg << 'Specialization missing'
        }
        if (!map.city) {
            retMsg << 'City missing'
        }
        if (!map.certificateNumber) {
            retMsg << 'Practicing Certificate Number missing'
        }
        if (!map.password) {
            retMsg << 'Password missing'
        }
        if (!map.confirmPassword) {
            retMsg << 'Confirm Password missing'
        }
        if (map.password && map.confirmPassword && map.password != map.confirmPassword) {
            retMsg << 'Password and Confirm Password not matching'
        }
        if (retMsg) {
            return [message: retMsg, validate: false]
        }
        [validate: true]
    }

    /**
     * Validates Request for Edit Profile
     * @param map
     * @return
     */
    def validateRequestForEdit( map ) {
        def retMsg = []
        if (!map.firstName) {
            retMsg << 'First Name missing'
        }
        if (!map.lastName) {
            retMsg << 'Last Name missing'
        }
        if (!map.primaryContact) {
            retMsg << 'Contact Number missing'
        }
        if (!map.emailAddress) {
            retMsg << 'Email Address missing'
        }
        if (!map.speciality) {
            retMsg << 'Specialization missing'
        }
        if (!map.expYear) {
            retMsg << 'Year of Exp missing'
        }
        if (!map.qualification) {
            retMsg << 'Qualification missing'
        }
        if (!map.city) {
            retMsg << 'City missing'
        }
        if (!Speciality.findByName( map.speciality )) {
            retMsg << 'Specialization not correct as maintained in System'
        }
        if (!map.certificateNumber) {
            retMsg << 'Practicing Certificate Number missing'
        }
        if (retMsg) {
            return [message: retMsg, validate: false]
        }
        [validate: true]
    }

    /**
     * Sent OTP to Mobile.. Temp Method TODO need to use right one
     * @param sentTo
     * @param name
     * @return
     */
    def sentOtpToMobileTemp( sentTo, name ) {
        def result = [success      : true,
                      message      : 'OTP sent successfully',
                      otp          : 'G765H',
                      generatedDate: new Date(),
                      statusCode   : '200',
                      messageId    : '23682637hjhjwqh'
        ]
        result
    }

    /**
     * Send Message to Mobile
     * @param sentTo
     * @param message
     * @return
     */
    def sentMessageToMobile( sentTo, message ) {
        logger.debug( 'In sentMessageToMobile' + sentTo + ' message ' + message )
        HttpClient client = new DefaultHttpClient();
        def result = [:]
        //Hi ${name}! Your Breath and Beat OTP is $otp"
        def template = URLEncoder.encode( "${message}", "UTF-8" )
        try {
            def generatedDate = new Date()
            String apiUrl = "${Holders.config.msg.sms.smsgupshup.url}/${Holders.config.msg.sms.smsgupshup.gateway.rest}" +
                    "?method=${Holders.config.msg.sms.smsgupshup.gateway.sendmethod}&send_to=$sentTo&msg=$template" +
                    "&msg_type=TEXT&userid=${Holders.config.msg.sms.smsgupshup.gateway.userid}&auth_scheme=plain" +
                    "&password=${Holders.config.msg.sms.smsgupshup.gateway.password}&v=1.1&format=text"
            HttpGet httpGet = new HttpGet( apiUrl );
            HttpResponse httpGetResponse = client.execute( httpGet )
            InputStreamReader inputStreamReader = new InputStreamReader( httpGetResponse.getEntity().content )
            String sendResult = inputStreamReader.getText()
            def arrayRes = sendResult.replace( "\n", "" ).split( "\\|" )
            if (arrayRes[0].trim() == 'success') {
                result = [success      : true,
                          message      : 'Message sent successfully',
                          generatedDate: generatedDate,
                          statusCode   : arrayRes[0].trim(),
                          messageId    : arrayRes[2].trim()
                ]
            } else if (arrayRes[0].trim() == 'error') {
                result = [success   : false,
                          message   : arrayRes[2].trim(),
                          statusCode: arrayRes[1].trim()]
            }
            if (httpGetResponse.statusLine?.statusCode != 200) {
                result = [success: false,
                          message: httpGetResponse.statusLine.reasonPhrase,
                          status : httpGetResponse.statusLine.statusCode]
            }
        } catch (Exception e) {
            result = [success: false,
                      message: e.message]
        }
        logger.debug( 'In sentMessageToMobile' + result )
        result
    }

    /**
     * Push Message to destination using Firebase
     * @param sentFrom
     * @param sentTo
     * @param message
     * @return
     */
    def pushFireBaseMessage( sentFrom, sentTo, message ) {
        logger.debug( 'In pushFireBaseMessage' + sentTo + ' message ' + message )
        HttpClient client = new DefaultHttpClient();
        def result = [:]
        try {
            String apiUrl = Holders.config.firebase.message.apiUrl
            HttpPost httpPost = new HttpPost( apiUrl );
            httpPost.setHeader( "Content-type", "application/json" )
            httpPost.setHeader( "Authorization", "key=${Holders.config.firebase.message.key}" )
            JSONObject jsonMessage = new JSONObject();
            FCMEntity fcmEntity = FCMEntity.findByDevicePhoneNumber( sentTo )
            if (!fcmEntity) {
                result = [success: false,
                          message: 'FCM number is not stored']
                return result
            }
            jsonMessage.put( "to", FCMEntity.findByDevicePhoneNumber( sentTo ).fcmNumber )
            jsonMessage.put( "priority", "high" )
            JSONObject notification = new JSONObject()
            notification.put( "title", "$sentFrom says.." )
            notification.put( "body", message )
            jsonMessage.put( "notification", notification )
            jsonMessage.put( "content_available", true )
            httpPost.setEntity( new StringEntity( jsonMessage.toString(), "UTF-8" ) );
            HttpResponse httpResponse = client.execute( httpPost );
            logger.debug( httpResponse );
            logger.debug( jsonMessage )
            result = [success: true,
                      message: 'Message sent successfully',
            ]
            if (httpResponse.statusLine?.statusCode != 200) {
                result = [success: false,
                          message: httpResponse.statusLine.reasonPhrase,
                          status : httpResponse.statusLine.statusCode]
            }
        } catch (Exception e) {
            result = [success: false,
                      message: e.message]
        }
        logger.debug( 'In pushFireBaseMessage' + result )
        result
    }

    /**
     * Generates OTP Key
     * @param len
     * @return
     */
    private char[] generateOtp( int len ) {
        String Capital_chars = "ABCDEFGHJKLMNPQRSTUVWXYZ";
        String numbers = "0123456789";
        String values = numbers + Capital_chars
        Random rndm_method = new Random();
        char[] password = new char[len];
        for (int i = 0; i < len; i++) {
            password[i] = values.charAt( rndm_method.nextInt( values.length() ) );
        }
        password;
    }
}
