<%@ page import="breath.beats.doctor.poc.app.Person" %>



<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'addressLine1', 'error')} required">
	<label for="addressLine1">
		<g:message code="person.addressLine1.label" default="Address Line1" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="addressLine1" required="" value="${personInstance?.addressLine1}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'addressLine2', 'error')} required">
	<label for="addressLine2">
		<g:message code="person.addressLine2.label" default="Address Line2" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="addressLine2" required="" value="${personInstance?.addressLine2}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'addressLine3', 'error')} required">
	<label for="addressLine3">
		<g:message code="person.addressLine3.label" default="Address Line3" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="addressLine3" required="" value="${personInstance?.addressLine3}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'age', 'error')} required">
	<label for="age">
		<g:message code="person.age.label" default="Age" />
		<span class="required-indicator">*</span>
	</label>
	<g:field name="age" type="number" value="${personInstance.age}" required=""/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'city', 'error')} required">
	<label for="city">
		<g:message code="person.city.label" default="City" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="city" required="" value="${personInstance?.city}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'country', 'error')} required">
	<label for="country">
		<g:message code="person.country.label" default="Country" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="country" required="" value="${personInstance?.country}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'dateOfBirth', 'error')} required">
	<label for="dateOfBirth">
		<g:message code="person.dateOfBirth.label" default="Date Of Birth" />
		<span class="required-indicator">*</span>
	</label>
	<g:datePicker name="dateOfBirth" precision="day"  value="${personInstance?.dateOfBirth}"  />

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'firstName', 'error')} required">
	<label for="firstName">
		<g:message code="person.firstName.label" default="First Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="firstName" required="" value="${personInstance?.firstName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'gender', 'error')} required">
	<label for="gender">
		<g:message code="person.gender.label" default="Gender" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="gender" required="" value="${personInstance?.gender}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'lastName', 'error')} required">
	<label for="lastName">
		<g:message code="person.lastName.label" default="Last Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="lastName" required="" value="${personInstance?.lastName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'maritalStatus', 'error')} required">
	<label for="maritalStatus">
		<g:message code="person.maritalStatus.label" default="Marital Status" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="maritalStatus" required="" value="${personInstance?.maritalStatus}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'middleName', 'error')} required">
	<label for="middleName">
		<g:message code="person.middleName.label" default="Middle Name" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="middleName" required="" value="${personInstance?.middleName}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'primaryContact', 'error')} required">
	<label for="primaryContant">
		<g:message code="person.primaryContant.label" default="Primary Contant" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="primaryContant" required="" value="${personInstance?.primaryContant}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'secondaryContact', 'error')} required">
	<label for="secondaryContact">
		<g:message code="person.secondaryContact.label" default="Secondary Contact" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="secondaryContact" required="" value="${personInstance?.secondaryContact}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'state', 'error')} required">
	<label for="state">
		<g:message code="person.state.label" default="State" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="state" required="" value="${personInstance?.state}"/>

</div>

<div class="fieldcontain ${hasErrors(bean: personInstance, field: 'zipCode', 'error')} required">
	<label for="zipCode">
		<g:message code="person.zipCode.label" default="Zip Code" />
		<span class="required-indicator">*</span>
	</label>
	<g:textField name="zipCode" required="" value="${personInstance?.zipCode}"/>

</div>

