
<%@ page import="breath.beats.doctor.poc.app.Person" %>
<!DOCTYPE html>
<html>
	<head>
		<meta name="layout" content="main">
		<g:set var="entityName" value="${message(code: 'person.label', default: 'Person')}" />
		<title><g:message code="default.list.label" args="[entityName]" /></title>
	</head>
	<body>
		<a href="#list-person" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Skip to content&hellip;"/></a>
		<div class="nav" role="navigation">
			<ul>
				<li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>
				<li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>
			</ul>
		</div>
		<div id="list-person" class="content scaffold-list" role="main">
			<h1><g:message code="default.list.label" args="[entityName]" /></h1>
			<g:if test="${flash.message}">
				<div class="message" role="status">${flash.message}</div>
			</g:if>
			<table>
			<thead>
					<tr>
					
						<g:sortableColumn property="addressLine1" title="${message(code: 'person.addressLine1.label', default: 'Address Line1')}" />
					
						<g:sortableColumn property="addressLine2" title="${message(code: 'person.addressLine2.label', default: 'Address Line2')}" />
					
						<g:sortableColumn property="addressLine3" title="${message(code: 'person.addressLine3.label', default: 'Address Line3')}" />
					
						<g:sortableColumn property="age" title="${message(code: 'person.age.label', default: 'Age')}" />
					
						<g:sortableColumn property="city" title="${message(code: 'person.city.label', default: 'City')}" />
					
						<g:sortableColumn property="country" title="${message(code: 'person.country.label', default: 'Country')}" />
					
					</tr>
				</thead>
				<tbody>
				<g:each in="${personInstanceList}" status="i" var="personInstance">
					<tr class="${(i % 2) == 0 ? 'even' : 'odd'}">
					
						<td><g:link action="show" id="${personInstance.id}">${fieldValue(bean: personInstance, field: "addressLine1")}</g:link></td>
					
						<td>${fieldValue(bean: personInstance, field: "addressLine2")}</td>
					
						<td>${fieldValue(bean: personInstance, field: "addressLine3")}</td>
					
						<td>${fieldValue(bean: personInstance, field: "age")}</td>
					
						<td>${fieldValue(bean: personInstance, field: "city")}</td>
					
						<td>${fieldValue(bean: personInstance, field: "country")}</td>
					
					</tr>
				</g:each>
				</tbody>
			</table>
			<div class="pagination">
				<g:paginate total="${personInstanceCount ?: 0}" />
			</div>
		</div>
	</body>
</html>
