package breathbeatsapi

import breath.beats.doctor.poc.app.*
import grails.util.Holders
import org.apache.log4j.Logger
import org.hibernate.Session

class ConsultationNotificationJob {
    def breathBeatsCommonService
    Logger logger = Logger.getLogger( this.class.name )
    static triggers = {
        simple name: 'mySimpleTrigger', startDelay: 60000, repeatInterval: 10000 // execute job once in 10 seconds
    }


    def execute() {
        logger.debug( 'Executing ConsultationNotificationJob ' )
        List<DoctorPatientConsultation> doctorPatientConsultations = DoctorPatientConsultation.findAllByConsultationStage( ConsultationStage.findByName( 'PAID' ) )
        logger.debug( 'Number of records to process ' + doctorPatientConsultations?.size() )
        doctorPatientConsultations?.each {doctorPatientConsultation ->
            Doctor doctor = doctorPatientConsultation.doctor
            if (doctor) {
                logger.debug( 'doctor ' + doctor.uuid )
                Person person = Person.findByUuid( doctor.uuid )
                DoctorPatientConsultationLog doctorPatientConsultationLog = DoctorPatientConsultationLog.findByDoctorPatientConsultationAndConsultationStage( doctorPatientConsultation, ConsultationStage.findByName( 'PAID' ) )
                if (doctorPatientConsultationLog) {
                    logger.debug( 'doctorPatientConsultationLog ' + doctorPatientConsultationLog )
                    long l1 = new Date().getTime()
                    long l2 = doctorPatientConsultationLog.createdOn.getTime()
                    long diff = l1 - l2;
                    long secondInMillis = 1000;
                    long elapsedSeconds = diff / secondInMillis;
                    Patient patient = doctorPatientConsultation.patient
                    if (elapsedSeconds > 3 * (Holders.config.consultation.delay.notification.timer)) {
                        logger.debug( 'Marking for refund ' )
                        new DoctorPatientConsultationLog( doctorPatientConsultation: doctorPatientConsultation, consultationStage: ConsultationStage.findByName( 'READY_TO_REFUND' ),
                                                          createdOn: new Date(), createdBy: breathBeatsCommonService.CREATED_BY )
                                .save( flush: true )
                        doctorPatientConsultation.consultationStage = ConsultationStage.findByName( 'READY_TO_REFUND' )
                        doctorPatientConsultation.save()
                    } else if (elapsedSeconds > 2 * (Holders.config.consultation.delay.notification.timer)) {
                        Date lastNotifiedConsultationAt = DoctorPatientConsultationLog.withSession {
                            Session session ->
                                session.createQuery( "select max(a.createdOn) from DoctorPatientConsultationLog a where a.consultationStage= :stage and a.doctorPatientConsultation=:consultation" )
                                        .setParameter( "stage", ConsultationStage.findByName( 'NOTIFIED' ) )
                                        .setParameter( "consultation", doctorPatientConsultation )
                                        .uniqueResult()
                        }
                        boolean sentNotification = true
                        if (lastNotifiedConsultationAt) {
                            l1 = new Date().getTime()
                            l2 = lastNotifiedConsultationAt.getTime()
                            diff = l1 - l2;
                            secondInMillis = 1000;
                            elapsedSeconds = diff / secondInMillis;
                            if (elapsedSeconds < Holders.config.consultation.delay.notification.timer) {
                                sentNotification = false
                            }
                        }
                        if (sentNotification) {
                            String notificationMessage = "Hi ${person.firstName}, Patient ${patient.name} is waiting for consultation. Reminder 2 . Please attend patient."
                            logger.debug( 'Sending Second notification ' + notificationMessage )
                            new DoctorPatientConsultationLog( doctorPatientConsultation: doctorPatientConsultation, consultationStage: ConsultationStage.findByName( 'NOTIFIED' ),
                                                              createdOn: new Date(), createdBy: breathBeatsCommonService.CREATED_BY )
                                    .save( flush: true )
                            breathBeatsCommonService.sentMessageToMobile( person.primaryContact, notificationMessage )
                        }
                    } else if (elapsedSeconds > Holders.config.consultation.delay.notification.timer) {
                        Date lastNotifiedConsultationAt = DoctorPatientConsultationLog.withSession {
                            Session session ->
                                session.createQuery( "select max(a.createdOn) from DoctorPatientConsultationLog a where a.consultationStage= :stage and a.doctorPatientConsultation=:consultation" )
                                        .setParameter( "stage", ConsultationStage.findByName( 'NOTIFIED' ) )
                                        .setParameter( "consultation", doctorPatientConsultation )
                                        .uniqueResult()
                        }
                        boolean sentNotification = true
                        if (lastNotifiedConsultationAt) {
                            l1 = new Date().getTime()
                            l2 = lastNotifiedConsultationAt.getTime()
                            diff = l1 - l2;
                            secondInMillis = 1000;
                            elapsedSeconds = diff / secondInMillis;
                            if (elapsedSeconds < Holders.config.consultation.delay.notification.timer) {
                                sentNotification = false
                            }
                        }
                        if (sentNotification) {
                            String notificationMessage = "Hi ${person.firstName}, Patient ${patient.name} is waiting for consultation. Reminder 1 . Please attend patient."
                            logger.debug( 'Sending first notification ' + notificationMessage )
                            new DoctorPatientConsultationLog( doctorPatientConsultation: doctorPatientConsultation, consultationStage: ConsultationStage.findByName( 'NOTIFIED' ),
                                                              createdOn: new Date(), createdBy: breathBeatsCommonService.CREATED_BY )
                                    .save( flush: true )
                            breathBeatsCommonService.sentMessageToMobile( person.primaryContact, notificationMessage )
                        }
                    }
                }
            }
        }

    }
}
