package breath.beats.doctor.poc.app

import com.breath.beats.doctor.app.common.BreathBeatsCommonService
import grails.converters.JSON
import grails.transaction.Transactional
import grails.util.Holders
import login.SecUser
import org.apache.log4j.Logger
import org.springframework.web.multipart.MultipartFile

@Transactional(readOnly = true)
class DoctorController {
    Logger logger = Logger.getLogger( this.class.name )
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def breathBeatsCommonService

    /**
     * Create speciality when Needed, May be Admin activity
     * Method : POST
     * Request JSON {"name", "Ortho", "description", "Ortho"}* @return
     */
    @Transactional
    def createSpeciality() {
        def map = request.JSON
        logger.debug( 'createSpeciality map ' + map )
        def result = [:]
        try {
            Speciality speciality = new Speciality( createdOn: new Date(),
                                                    createdBy: 'SYSTEM',
                                                    name: map.name,
                                                    description: map.description
            )
            if (speciality.validate()) {
                Speciality savedSpeciality = speciality.save( flush: true )
                result = [savedUser: savedSpeciality, success: true]
            } else {
                result = [savedUser: null, success: false, message: 'Invalid input']
            }

        } catch (Throwable e) {
            e.printStackTrace()
            result = [savedUser: null, success: false, message: e.message]
            logger.debug( 'createSpeciality result ' + result )
            render result as JSON
        }
        logger.debug( 'createSpeciality result ' + result )
        render result as JSON
    }

    /**
     * Loads Consultation log
     * Method: POST
     * Request JSON {"uuid", "D9823927"}* @return
     */
    def loadConsultationLog() {
        def map = request.JSON
        logger.debug( 'loadPatientNotification map ' + map )
        def uuid = map.uuid
        List<DoctorPatientConsultation> consultations = DoctorPatientConsultation.findAllByDoctor( Doctor.findByUuid( uuid ) )
        consultations.sort {it.createdOn}
        consultations = consultations.reverse( true )
        def result = consultations.collect {DoctorPatientConsultation it ->
            [name               : it.patient.name,
             patientEmailAddress: it.patient.emailAddress,
             patientPhone       : it.patient.contactNumber,
             consStartDate      : it.consStartDate,
             stage              : it.consultationStage.name,
             mode               : it.consultationMode.name,
             consultationId     : it.id
            ]
        }
        logger.debug( 'loadPatientNotification result ' + result )
        render result as JSON
    }

    /**
     * Loads Doctor Profile
     * Method: POST
     * Request JSON :{'hospitalNumber':'D019200002'}* @return
     */
    def loadDoctorProfileByHospitalNumber() {
        def map = request.JSON
        logger.debug( 'lodDoctorProfileByHospitalNumber map ' + map )
        def hospitalNumber = map.uuid
        def finalResult = [:]
        def result = [:]
        Person person = Person.findByUuid( hospitalNumber )
        if (person) {
            Doctor doctor = Doctor.findByUuid( hospitalNumber )
            DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
            result.name = BreathBeatsCommonService.DR_NAME_PREFIX + person.firstName + ' ' + person.lastName
            result.firstName = person.firstName
            result.uuid = person.uuid
            result.lastName = person.lastName
            result.city = person.city
            result.zipCode = person.zipCode
            result.emailAddress = person.emailAddress
            result.primaryContact = person.primaryContact
            result.speciality = doctor.speciality.name
            result.certificateNumber = doctor.certificateNumber
            result.qualification = doctor.qualification
            result.certificateImgLocUrl = doctor.certificateImgLoc
            result.doctorImageUrl = doctorProfile.doctorImage
            result.status = doctorProfile.status.name
            result.expYear = doctorProfile.expYear
            List hospitals = doctorProfile.hospitalWorkedIn?.split( "," )
            int i = 1
            hospitals?.each {
                result.put( "hospital${i++}", it )
            }
            finalResult = [success: true, result: result]
        } else {
            finalResult = [success: false, result: result, message: 'Record not present']
        }
        logger.debug( 'lodDoctorProfileByHospitalNumber result ' + finalResult )
        render finalResult as JSON
    }

    /**
     * Load current status and Fee information
     * Method: POST
     * Request JSON:"{uuid:'D019200002',}"
     * @return
     */
    def loadCurrentStatusAndFeeInformation() {
        def map = request.JSON
        logger.debug( 'loadCurrentStatusAndFeeInformation map ' + map )
        def hospitalNumber = map.uuid
        def finalResult = [:]
        def result = [:]
        Person person = Person.findByUuid( hospitalNumber )
        if (person) {
            Doctor doctor = Doctor.findByUuid( hospitalNumber )
            DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
            result.currentStatus = doctorProfile.currentStatus?.name
            result.feeForPatient = doctorProfile.feeForPatient ?: ''
            result.feeForDiscussion = doctorProfile.feeForDiscussion ?: ''
            result.timeDurationForDiscussion = doctorProfile.timeDurationForDiscussion ?: Holders.config.consultation.fee.timePeriod.default
            result.timeDurationForPatient = doctorProfile.timeDurationForPatient ?: Holders.config.consultation.fee.timePeriod.default
            finalResult = [success: true, result: result]
        } else {
            finalResult = [success: false, result: result, message: 'Record not present']
        }
        logger.debug( 'loadCurrentStatusAndFeeInformation result ' + finalResult )
        render finalResult as JSON
    }

    /**
     * Edit current status of doctor
     * Method: POST
     * Request JSON:"{'uuid' :'D2378279873'
     'currentStatus':'Available',
     'feeForPatient':300,
     'timeDurationForPatient':30,
     'feeForDiscussion':200,
     'timeDurationForDiscussion':30}"
     * @return
     */
    @Transactional
    def updateDoctorProfileCurrentStatusOrFee() {
        def map = request.JSON
        logger.debug( 'updateDoctorProfileCurrentStatus map ' + map )
        def result = [:]
        Person existingPerson = Person.findByUuid( map.uuid )
        if (!existingPerson) {
            result.message = 'Profile Not found'
            result.success = false
            logger.debug( 'updateDoctorProfileCurrentStatus result ' + result )
            render result as JSON
            return
        }
        def existingDoctorProfile = DoctorProfile.findByDoctor( Doctor.findByUuid( map.uuid ) )
        if (existingDoctorProfile) {
            existingDoctorProfile.currentStatus = Status.findByName( map.currentStatus )
            existingDoctorProfile.feeForPatient = map.feeForPatient
            existingDoctorProfile.feeForDiscussion = map.feeForDiscussion
            existingDoctorProfile.timeDurationForDiscussion = map.timeDurationForDiscussion
            existingDoctorProfile.timeDurationForPatient = map.timeDurationForPatient
        }
        result = [uuid   : map.uuid,
                  message: 'Status/Fee change complete',
                  success: true]
        logger.debug( 'updateDoctorProfileCurrentStatusOrFee result ' + result )
        render result as JSON
    }


    private String getCommaSeparatedString( String str1, String str2, String str3 ) {
        String retString = ''
        if (str1) {
            retString += str1
        }
        if (str2) {
            retString += ',' + str2
        }
        if (str3) {
            retString += ',' + str3
        }
        retString
    }

    /**
     * Edit Doctor profile
     * Method: POST
     * JSON : "{""firstName"": ""Shiv"",
     ""uuid"",""D23283782"",
     ""lastName"": ""Tiwari"",
     ""city"": null,
     ""emailAddress"": ""shiv.tiwari@gmail.com"",
     ""primaryContact"": ""9901599229"",
     ""speciality"": ""Ortho"",
     ""certificateNumber"": ""BLR78781"",
     ""qualification"": null,
     ""expYear':4,
     ""hospital1"",""Manipal"",
     ""hospital2"":""Fortis"",
     ""hospital3"",""Appolo"",
     ""certification1"",""certification1"",
     ""certification2"",""certification2"",
     ""certification3"",""certification3"",
     ""degreesHeld1"",""degreesHeld1"",
     ""degreesHeld2"",""degreesHeld2"",
     ""degreesHeld3"",""degreesHeld3"",
     ""award1"",""award1"",
     ""award2"",""award2"",
     ""award3"",""award3"",
     ""homeCareAvailability"":""Y"",
     ""clinicContantNumber"",""23287638"",
     ""clinicAddress"", ""232/4, HSR,  Bangalore""}"
     * @return
     */
    @Transactional
    def editDoctorProfile() {
        def map = request.JSON
        boolean sendOpt = false
        logger.debug( 'editDoctorProfile map ' + map )
        def result = [:]
        def validationResult = breathBeatsCommonService.validateRequestForEdit( map )
        if (!validationResult.validate) {
            result = validationResult
            result.success = false
            logger.debug( 'editDoctorProfile result ' + result )
            render result as JSON
            return
        }
        Person existingPerson = Person.findByUuid( map.uuid )
        if (!existingPerson) {
            result.message = 'Profile Not found'
            result.success = false
            logger.debug( 'editDoctorProfile result ' + result )
            render result as JSON
            return
        }
        if (existingPerson) {
            SecUser secUser = SecUser.findByUsername( existingPerson.primaryContact )
            if (existingPerson.primaryContact != map.primaryContact) {
                secUser.username = map.primaryContact
            } else {
                secUser.firstName = map.firstName
                secUser.lastName = map.lastName
            }
            if (map.password && map.password.length() > 0) {
                secUser.password = map.password
            }
            if (secUser.validate()) {
                secUser.save( flush: true )
            } else {
                result.message = 'Invalid data'
                result.success = false
                logger.debug( 'editDoctorProfile result ' + result )
                render result as JSON
                return
            }
            Doctor doctor = Doctor.findByUuid( map.uuid )
            def existingDoctorProfile = DoctorProfile.findByDoctor( doctor )
            if (existingDoctorProfile) {
                existingDoctorProfile.expYear = map.expYear
                existingDoctorProfile.hospitalWorkedIn = getCommaSeparatedString( map.hospital1, map.hospital2, map.hospital3 )
                existingDoctorProfile.createdOn = new Date()
                existingDoctorProfile.createdBy = breathBeatsCommonService.CREATED_BY
                if (existingDoctorProfile.validate()) {
                    existingDoctorProfile.save( flash: true )
                } else {
                    result.message = 'Invalid data'
                    result.success = false
                    logger.debug( 'editDoctorProfile result ' + result )
                    render result as JSON
                    return
                }
            } else {
                result = [message: 'Your Profile is not present.', success: false]
                logger.debug( 'editDoctorProfile result ' + result )
                render result as JSON
                return
            }
            doctor.speciality = Speciality.findByName( map.speciality )
            doctor.qualification = map.qualification
            if (existingDoctorProfile.status != Status.findByName( 'Approved' )) {
                doctor.certificateNumber = map.certificateNumber
                doctor.certificateImgLoc = map.certificateImgLocUrl
            }
            doctor.createdBy = breathBeatsCommonService.CREATED_BY
            doctor.createdOn = new Date()
            if (doctor.validate()) {
                doctor.save( flush: true )
            } else {
                result.message = 'Invalid data'
                result.success = false
                logger.debug( 'editDoctorProfile result ' + result )
                render result as JSON
                return
            }
            existingPerson.firstName = map.firstName
            existingPerson.middleName = map.middleName
            existingPerson.lastName = map.lastName
            existingPerson.emailAddress = map.emailAddress
            if (existingPerson.primaryContact != map.primaryContact) {
                sendOpt = true
            }
            existingPerson.primaryContact = map.primaryContact
            existingPerson.city = map.city
            existingPerson.createdBy = breathBeatsCommonService.CREATED_BY
            existingPerson.createdOn = new Date()
            if (existingPerson.validate()) {
                existingPerson.save( flush: true )
            } else {
                result.message = 'Invalid data'
                result.success = false
                logger.debug( 'editDoctorProfile result ' + result )
                render result as JSON
                return
            }
        }
        if (sendOpt) {
            def otpResult = breathBeatsCommonService.sentOtpToMobileTemp( existingPerson.primaryContact, existingPerson.firstName )
            if (!otpResult.success) {
                if (otpResult.statusCode in [101, 105, 175]) {
                    result = [message: 'Primary Contact is not valid.' + otpResult.message, success: false]
                    logger.debug( 'editDoctorProfile result ' + result )
                    render result as JSON
                }
            } else {
                AuthTokens authTokens = new AuthTokens(
                        key: existingPerson.uuid,
                        token: otpResult.otp,
                        tokenType: 'SMS',
                        generatedOn: otpResult.generatedDate,
                        createdBy: breathBeatsCommonService.CREATED_BY,
                        createdOn: new Date(),
                        status: 'GENERATED'
                )
                if (authTokens.validate()) {
                    authTokens.save( flush: true )
                }
                result = [uuid   : existingPerson.uuid,
                          otpSent: true,
                          message: 'Edit complete. OTP has been sent to your primary contact. ' +
                                  'Please use your primary contact as login',
                          success: true]
                logger.debug( 'editDoctorProfile result ' + result )
                render result as JSON
            }
        } else {
            result = [uuid   : existingPerson.uuid,
                      message: 'Edit complete. Please use your primary contact as login',
                      success: true]
            logger.debug( 'editDoctorProfile result ' + result )
            render result as JSON
        }
    }

    /**
     * API to list Specialities
     * @return
     */
    def listSpeciality() {
        def result = Speciality.list()
        result = result?.collect {
            [
                    "description": it.description,
                    "name"       : it.name
            ]
        }
        logger.debug( 'listSpeciality result ' + result )
        render result as JSON
    }

    /**
     * API to upload Documents while consultation
     * Method: POST
     * @return
     */
    def uploadDocumentForConsultation() {
        def map = request.parameterMap
        logger.debug( 'uploadDocumentForConsultation map ' + map )
        def consultationId = map.cunsultationId?.getAt( 0 )
        MultipartFile file = (MultipartFile) request.multipartFiles.file?.get( 0 )
        def result = [success: true]
        if (!consultationId) {
            result.message = 'Consultation id is required'
            result.success = false
            logger.debug( 'uploadDocumentForConsultation result ' + result )
            render result as JSON
        }
        if (DoctorPatientConsultation.findById( consultationId )) {//TODO reverse the condition
            result.message = 'Consultation id is not present'
            result.success = false
            logger.debug( 'uploadDocumentForConsultation result ' + result )
            render result as JSON
        }
        def fileName = map.fileName
        result = breathBeatsCommonService.uploadDocumentToAWSS3( Holders.config.s3.server.access.consultation.loc + breathBeatsCommonService.SUFFIX + consultationId, file )
        logger.debug( 'uploadDocumentForConsultation result ' + result )
        render result as JSON
    }

    /**
     * API to Download document uploaded for consultation
     * Method: GET
     * @return
     */
    def downloadDocumentsForConsultation() {
        def map = request.parameterMap
        logger.debug( 'downloadDocumentsForConsultation map ' + map )
        def consultationId = map.cunsultationId
        def result = [success: true]
        if (!consultationId) {
            result.message = 'Consultation id is required'
            result.success = false
            logger.debug( 'downloadDocumentsForConsultation result ' + result )
            render result as JSON
        }
        DoctorPatientConsultation doctorPatientConsultation = DoctorPatientConsultation.findById( consultationId )
        if (!doctorPatientConsultation) {
            result.message = 'Consultation id is not present '
            result.success = false
            logger.debug( 'downloadDocumentsForConsultation result ' + result )
            render result as JSON
        }
        try {
            def fileName = 'doctor-consultations/34673437483/IMG_20180225_213048.jpg'
//doctorPatientConsultation.fileName TODO Need to implement to complete this
            result = breathBeatsCommonService.downloadDocumentFromAWSS3( Holders.config.s3.server.access.consultation.loc + breathBeatsCommonService.SUFFIX + consultationId, fileName )
            if (!result.success) {
                logger.debug( 'downloadDocumentsForConsultation result ' + result )
                render result as JSON
            }
            def extension = breathBeatsCommonService.getExtensionFromFilenameLowercase( fileName )
            def contentType = Holders.config.grails.mime.types[extension]
            def inputStreamByte = result.fileContent
            response.setHeader( 'Content-length', inputStreamByte.length.toString() )
            response.contentType = contentType
            response.setHeader( 'Content-disposition', 'attachment;filename=' + fileName )
            response.setHeader( 'Expires', '0' );
            response.setHeader( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0' );
            response.setHeader( 'Pragma', 'public' );
            response.getOutputStream().write( inputStreamByte )
            response.outputStream.flush()
        }
        catch (Throwable t) {
            response.sendError( 400, 'Error while loading the profile image' )
        }
        logger.debug( 'downloadDocumentsForConsultation result ' + result )
        render result as JSON
    }

    /**
     * API to Download image uploaded for profile
     * Method: GET
     * @return
     */
    @Transactional
    def uploadProfileImage() {
        def map = request.parameterMap
        logger.debug( 'uploadProfileImage map ' + map )
        def uuid = map.uuid?.getAt( 0 )
        MultipartFile file = (MultipartFile) request.multipartFiles.file?.get( 0 )
        def result = [success: true]
        int imageMaxAllowedSize = Holders.config.profile.image.max.alowed.size
        if (!uuid) {
            result.message = 'Doctor UUID id is required'
            result.success = false
            logger.debug( 'uploadProfileImage result ' + result )
            render result as JSON
            return
        }
        if (!(file.contentType.substring( file.contentType.lastIndexOf( '/' ) + 1 ) in ['png', 'gif', 'jpeg', 'jpg', 'bmp'])) {
            result.message = 'Image format should be among [png, gif, jpeg, jpg, bmp]'
            result.success = false
            logger.debug( 'uploadProfileImage result ' + result )
            render result as JSON
            return
        }
        Doctor doctor = Doctor.findByUuid( uuid )
        if (!doctor) {
            result.message = 'Doctor profile is not present'
            result.success = false
            logger.debug( 'uploadProfileImage result ' + result )
            render result as JSON
            return
        }
        logger.debug( 'uploaded file size  ' + file.size )
        if (file.size > imageMaxAllowedSize) {
            result.message = "Image size ${file.size} should not be more than ${imageMaxAllowedSize / 1024} KB"
            result.success = false
            logger.debug( 'uploadProfileImage result ' + result )
            render result as JSON
            return
        }
        result = breathBeatsCommonService.uploadDocumentToAWSS3( Holders.config.s3.server.access.profile.image.loc + breathBeatsCommonService.SUFFIX + uuid, file )
        if (result.success) {
            DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
            doctorProfile.doctorImage = result.url
            doctorProfile.save( flush: true )
        }
        logger.debug( 'uploadProfileImage result ' + result )
        render result as JSON
    }

    /**
     * API to Upload profile certificate
     * Method: POST
     * @return
     */
    @Transactional
    def uploadProfileCertificate() {
        def map = request.parameterMap
        logger.debug( 'uploadProfileCertificate map ' + map )
        def uuid = map.uuid?.getAt( 0 )
        MultipartFile file = (MultipartFile) request.multipartFiles.file?.get( 0 )
        def result = [success: true]
        if (!uuid) {
            result.message = 'Doctor UUID id is required'
            result.success = false
            logger.debug( 'uploadProfileCertificate result ' + result )
            render result as JSON
        }
        Doctor doctor = Doctor.findByUuid( uuid )
        if (!doctor) {
            result.message = 'Doctor profile is not present'
            result.success = false
            logger.debug( 'uploadProfileCertificate result ' + result )
            render result as JSON
        }
        result = breathBeatsCommonService.uploadDocumentToAWSS3( Holders.config.s3.server.access.profile.image.loc + breathBeatsCommonService.SUFFIX + uuid, file )
        if (result.success) {
            doctor.certificateImgLoc = result.url
            doctor.save( flush: true )
        }
        logger.debug( 'uploadProfileCertificate result ' + result )
        render result as JSON
    }

    /**
     * API to Download profile Image
     * Method: GET
     * @return
     */
    def downloadProfileImage() {
        def map = request.parameterMap
        logger.debug( 'downloadProfileImage map ' + map )
        def uuid = map.uuid
        def result = [success: true]
        if (!uuid) {
            result.message = 'Doctor UUID id is required'
            result.success = false
            render result as JSON
            return
        }
        DoctorProfile doctorProfile = DoctorProfile.findByDoctor( Doctor.findByUuid( uuid ) )
        if (!doctorProfile) {
            result.message = 'Doctor profile is not present'
            result.success = false
            logger.debug( 'downloadProfileImage result ' + result )
            render result as JSON
            return
        }
        try {
            def fileName = doctorProfile.doctorImage
            if (!fileName) {
                result.message = 'Doctor profile image is not present'
                result.success = false
                logger.debug( 'downloadProfileImage result ' + result )
                render result as JSON
                return
            }
            result = breathBeatsCommonService.downloadDocumentFromAWSS3( Holders.config.s3.server.access.profile.image.loc + breathBeatsCommonService.SUFFIX + uuid, fileName )
            if (!result.success) {
                logger.debug( 'downloadProfileImage result ' + result )
                render result as JSON
                return
            }
            def extension = breathBeatsCommonService.getExtensionFromFilenameLowercase( fileName )
            def contentType = Holders.config.grails.mime.types[extension]
            def inputStreamByte = result.fileContent
            response.setHeader( 'Content-length', inputStreamByte.length.toString() )
            response.contentType = contentType
            response.getOutputStream().write( inputStreamByte )
            response.outputStream << inputStreamByte
            response.outputStream.flush()
        }
        catch (Throwable t) {
            response.sendError( 400, 'Error while loading the profile image' )
        }
    }

    /**
     * API to associate doctor phone number with Firebase Communication Message code ( FCM)
     * Method: POST
     * Request JSON "{
     uuid:'D019200002',
     }"
     * @return
     */
    @Transactional
    def syncDoctorFCM() {
        def map = request.JSON
        def uuid = map.uuid
        def fcm = map.fcm
        def result = [success: true]
        if (!uuid) {
            result.message = 'Doctor UUID id is required'
            result.success = false
            logger.debug( 'syncDoctorFCM result ' + result )
            render result as JSON
            return
        }
        if (!fcm) {
            result.message = 'FCM number is required'
            result.success = false
            logger.debug( 'syncDoctorFCM result ' + result )
            render result as JSON
            return
        }
        Person person = Person.findByUuid( uuid )
        if (!person) {
            result.message = 'Doctor is not present'
            result.success = false
            logger.debug( 'syncDoctorFCM result ' + result )
            render result as JSON
            return
        }
        FCMEntity fcmEntity = FCMEntity.findByDevicePhoneNumber( person.primaryContact )
        if (!fcmEntity) {
            fcmEntity = new FCMEntity( createdOn: new Date(), createdBy: breathBeatsCommonService.CREATED_BY, devicePhoneNumber: person.primaryContact, fcmNumber: fcm )
            if (fcmEntity.validate()) {
                fcmEntity.save( flush: true )
            } else {
                result.message = 'Error while Syncing FCM number'
                result.success = false
                logger.debug( 'syncDoctorFCM result ' + result )
                render result as JSON
                return
            }
        }
        result.message = 'FCM is updated for device'
        result.success = true
        logger.debug( 'syncDoctorFCM result ' + result )
        render result as JSON
    }

    /**
     * API to associate patient phone number with Firebase Communication Message code ( FCM)
     * Method: POST
     * Request JSON :"{
     ""patientContactNumber"":'92329739898',
     }"
     * @return
     */
    @Transactional
    def syncPatientFCM() {
        def map = request.JSON
        String patientContactNumber = map.patientContactNumber
        def fcm = map.fcm
        def result = [success: true]
        if (!patientContactNumber) {
            result.message = 'Patient phone number is required'
            result.success = false
            logger.debug( 'syncPatientFCM result ' + result )
            render result as JSON
            return
        }
        Patient patient = Patient.findByContactNumber( patientContactNumber )
        if (!patient) {
            result.message = 'patient is not present'
            result.success = false
            logger.debug( 'syncPatientFCM result ' + result )
            render result as JSON
            return
        }
        if (!fcm) {
            result.message = 'FCM number is required'
            result.success = false
            logger.debug( 'syncDoctorFCM result ' + result )
            render result as JSON
            return
        }
        FCMEntity fcmEntity = FCMEntity.findByDevicePhoneNumber( patient.contactNumber )
        if (!fcmEntity) {
            fcmEntity = new FCMEntity( createdOn: new Date(), createdBy: breathBeatsCommonService.CREATED_BY, devicePhoneNumber: patient.contactNumber, fcmNumber: fcm )
            if (fcmEntity.validate()) {
                fcmEntity.save( flush: true )
            } else {
                result.message = 'Error while Syncing FCM number'
                result.success = false
                logger.debug( 'syncDoctorFCM result ' + result )
                render result as JSON
                return
            }
        }
        result.message = 'FCM is updated for device'
        result.success = true
        logger.debug( 'syncDoctorFCM result ' + result )
        render result as JSON
    }

    /**
     * API to download profile Certificate
     * Method: GET
     * @return
     */
    def downloadProfileCertificate() {
        def map = request.parameterMap
        logger.debug( 'downloadProfileCertificate map ' + map )
        def uuid = map.uuid
        def result = [success: true]
        if (!uuid) {
            result.message = 'Doctor UUID id is required'
            result.success = false
            logger.debug( 'downloadProfileCertificate result ' + result )
            render result as JSON
            return
        }
        Doctor doctor = Doctor.findByUuid( uuid )
        if (!doctor) {
            result.message = 'Doctor is not present'
            result.success = false
            logger.debug( 'downloadProfileCertificate result ' + result )
            render result as JSON
            return
        }
        try {
            def fileName = doctor.certificateImgLoc
            if (!fileName) {
                result.message = 'Doctor certificate is not present'
                result.success = false
                logger.debug( 'downloadProfileCertificate result ' + result )
                render result as JSON
                return
            }
            result = breathBeatsCommonService.downloadDocumentFromAWSS3( Holders.config.s3.server.access.profile.image.loc + breathBeatsCommonService.SUFFIX + uuid, fileName )
            if (!result.success) {
                logger.debug( 'downloadProfileCertificate result ' + result )
                render result as JSON
                return
            }
            def extension = breathBeatsCommonService.getExtensionFromFilenameLowercase( fileName )
            def contentType = Holders.config.grails.mime.types[extension]
            def inputStreamByte = result.fileContent
            response.setHeader( 'Content-length', inputStreamByte.length.toString() )
            response.contentType = contentType
            response.setHeader( 'Content-disposition', 'attachment;filename=' + fileName )
            response.setHeader( 'Expires', '0' );
            response.setHeader( 'Cache-Control', 'must-revalidate, post-check=0, pre-check=0' );
            response.setHeader( 'Pragma', 'public' );
            response.getOutputStream().write( inputStreamByte )
            response.outputStream.flush()
        }
        catch (Throwable t) {
            response.sendError( 400, 'Error while loading the profile image' )
        }
    }
}
