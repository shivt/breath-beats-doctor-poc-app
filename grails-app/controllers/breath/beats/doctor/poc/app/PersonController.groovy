package breath.beats.doctor.poc.app

import grails.converters.JSON
import grails.transaction.Transactional
import org.apache.log4j.Logger

@Transactional(readOnly = true)
class PersonController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def breathBeatsCommonService
    Logger logger = Logger.getLogger( this.class.name )

    /**
     * Lists All Persons
     * Method ; GET
     * @return
     */
    def list() {
        def persons = Person.list( [max: Integer.MAX_VALUE] )?.sort() {it.firstName}.sort() {it.lastName}
        def map = [list: persons, count: Person.count()]
        render map as JSON
    }
}
