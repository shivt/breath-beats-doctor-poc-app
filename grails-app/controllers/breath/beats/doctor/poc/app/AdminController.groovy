package breath.beats.doctor.poc.app

import grails.converters.JSON
import grails.transaction.Transactional
import org.apache.log4j.Logger

@Transactional(readOnly = true)
class AdminController {
    Logger logger = Logger.getLogger( this.class.name )
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    /**
     * API to approve Doctor's Profile.
     * Method: POST
     * Request JSON {"phoneNumber":"2372328795"}
     * @return
     */
    @Transactional
    def approveDoctor() {
        def map = request.JSON
        logger.debug( 'approveDoctor map ' + map )
        def result = [:]
        try {
            Person person = Person.findByPrimaryContact( map.phoneNumber )
            if (!person) {
                result = [success: false, message: 'Person not present for ' + map.phoneNumber]
                render result as JSON
                return
            }
            Doctor doctor = Doctor.findByUuid( person.uuid )
            if (!doctor) {
                result = [success: false, message: 'Doctor not present for ' + map.phoneNumber]
                render result as JSON
                return
            }
            if (!doctor.certificateImgLoc || !doctor.certificateNumber || !doctor.qualification) {
                result = [success: false, message: 'Doctor not eligible for approval missing certificate image or qualification or certificate number ']
                render result as JSON
                return
            }
            DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )


            if (!doctorProfile) {
                result = [success: false, message: 'Doctor profile is not present for ' + map.phoneNumber]
            } else if (!doctorProfile.expYear) {
                result = [success: false, message: 'Doctor not eligible for approval missing doctor years of experience']
            } else if (doctorProfile.status in [Status.findByName( 'Draft' )]) {
                doctorProfile.status = Status.findByName( 'Approved' )
                if (!doctorProfile.currentStatus) {
                    doctorProfile.currentStatus = Status.findByName( 'Available' )
                }
                doctorProfile.save( flush: true )
                result = [success: false, message: 'Doctor is approved']
            } else {
                result = [success: false, message: 'Doctor is already approved']
            }

        } catch (Throwable e) {
            e.printStackTrace()
            result = [success: false, message: e.message]
            render result as JSON
        }
        logger.debug( 'approveDoctor result ' + result )
        render result as JSON
    }

    /**
     * API to Reject doctor's Profile
     * Method: POST
     * Request JSON {"phoneNumber":"2372328795"}
     * @return
     */
    @Transactional
    def rejectDoctorProfile() {
        def map = request.JSON
        logger.debug( 'rejectDoctorProfile map ' + map )
        def result = [:]
        try {
            Person person = Person.findByPrimaryContact( map.phoneNumber )
            if (!person) {
                result = [success: false, message: 'Person not present for ' + map.phoneNumber]
                render result as JSON
                return
            }
            Doctor doctor = Doctor.findByUuid( person.uuid )
            if (!doctor) {
                result = [success: false, message: 'Doctor not present for ' + map.phoneNumber]
                render result as JSON
                return
            }
            DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
            if (!doctorProfile) {
                result = [success: false, message: 'Doctor profile is not present for ' + map.phoneNumber]
            } else if (doctorProfile.status in [Status.findByName( 'Approved' ), Status.findByName( 'Draft' )]) {
                doctorProfile.status = Status.findByName( 'Rejected' )
                doctorProfile.save( flush: true )
                result = [success: true, message: 'Doctor profile is rejected']
            } else {
                result = [success: false, message: 'Doctor is already inactive']
            }

        } catch (Throwable e) {
            e.printStackTrace()
            result = [success: false, message: e.message]
            logger.debug( 'rejectDoctorProfile result ' + result )
            render result as JSON
        }
        logger.debug( 'rejectDoctorProfile result ' + result )
        render result as JSON
    }

    /**
     * API to List doctor Profile. This will list Doctor, his/her approval status and current status
     * Method: GET
     * @return
     */
    def listDoctors() {
        List<Doctor> doctors = Doctor.list()
        def finalResult = []
        doctors.each {
            def result = [:]
            result.uuid = it.uuid
            Person person = Person.findByUuid( it.uuid )
            result.contactNumber = person?.primaryContact
            DoctorProfile doctorProfile = DoctorProfile.findByDoctor( it )
            result.firtName = person.firstName
            result.lastName = person.lastName
            result.approvalStatus = doctorProfile?.status?.name
            result.currentStatus = doctorProfile?.currentStatus?.name
            finalResult.add( result )
        }
        logger.debug( 'lstDoctors finalResult ' + finalResult )
        render finalResult as JSON
    }

    /**
     * API to Create Speciality If needed
     * Method: POST
     * @return
     */
    @Transactional
    def createSpeciality() {
        def map = request.JSON
        logger.debug( 'createSpeciality map ' + map )
        def result = [:]
        try {
            Speciality speciality = Speciality.findByNameIlike( map.name )
            if (speciality) {
                result = [success: false, message: "Speciality ${map.name} already preset in system. "]
            } else {
                speciality = new Speciality(
                        name: map.name,
                        description: map.description,
                        createdBy: 'SYSTEM',
                        createdOn: new Date()
                )
                if (speciality.validate()) {
                    speciality = speciality.save()
                } else {
                    result = [success: false, message: "Invalid request"]
                }
                result = [success: true, message: 'Speciality is been created']
            }
        } catch (Throwable e) {
            e.printStackTrace()
            result = [success: false, message: e.message]
            logger.debug( 'createSpeciality result ' + result )
            render result as JSON
        }
        logger.debug( 'createSpeciality result ' + result )
        render result as JSON
    }


}
