package breath.beats.doctor.poc.app

import grails.converters.JSON
import grails.transaction.Transactional
import grails.util.Holders
import org.apache.log4j.Logger

@Transactional(readOnly = true)
class CommunicationController {

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    Logger logger = Logger.getLogger( this.class.name )
    def breathBeatsCommonService

    /**
     * Send OTP to mobile
     * Method: POST
     * Request JSON {"uuid":"D979787987"}
     * @return
     */
    def setOtpToUser() {
        def map = request.JSON
        logger.debug( 'setOtpToUser map ' + map )
        def uuid = map.uuid
        def result = [:]
        if (uuid) {
            Person person = Person.findByUuid( uuid )
            if (person) {
                def contactNumber = person.primaryContact
                result = breathBeatsCommonService.sentOtpToMobileTemp( contactNumber, person.firstName )
            } else {
                result = [success: false, message: 'Person not Present']
            }
        } else {
            result = [success: false, message: 'UUID not present']
        }
        logger.debug( 'setOtpToUser result ' + result )
        render result as JSON
    }

    /**
     * Verify OTP and Mobile
     * Method: POST
     * Request JSON {"uuid":"D22878787", "otp":"626267"}
     * @return
     */
    def verifyOpt() {
        def map = request.JSON
        logger.debug( 'setOtpToUser map ' + map )
        def uuid = map.uuid
        def otp = map.otp
        def result = [:]
        if (!otp) {
            result = [success: false, message: 'otp not present in request']
        }
        if (uuid) {
            Person person = Person.findByUuid( uuid )
            if (person) {
                AuthTokens authTokens = AuthTokens.findByKeyAndTokenAndStatus( uuid, otp, 'GENERATED' )
                if (authTokens) {
                    long l1 = new Date().getTime()
                    long l2 = authTokens.generatedOn.getTime()
                    long diff = l2 - l1;
                    long secondInMillis = 1000;
                    long elapsedSeconds = diff / secondInMillis;
                    if (elapsedSeconds > Holders.config.otp.expiration.interval) {
                        authTokens.status = 'EXPIRED'
                        authTokens.createdOn = new Date()
                        authTokens.createdBy = breathBeatsCommonService.CREATED_BY
                        authTokens.save( flush: true )
                        result = [success: false, message: 'OTP is either expired ']
                    } else {
                        authTokens.status = 'VERIFIED'
                        authTokens.createdOn = new Date()
                        authTokens.createdBy = breathBeatsCommonService.CREATED_BY
                        authTokens.save( flush: true )
                    }
                } else {
                    result = [success: false, message: 'OTP is either expired or already verified']
                }
            } else {
                result = [success: false, message: 'Person Not Present']
            }
        } else {
            result = [success: false, message: 'UUID not present']
        }
        render result as JSON
    }

}
