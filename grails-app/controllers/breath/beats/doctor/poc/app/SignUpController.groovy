package breath.beats.doctor.poc.app

import grails.converters.JSON
import grails.transaction.Transactional
import login.SecRole
import login.SecUser
import login.SecUserSecRole
import org.apache.log4j.Logger
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder

@Transactional(readOnly = true)
class SignUpController {
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def breathBeatsCommonService
    BCryptPasswordEncoder bCryptPasswordEncoder
    Logger logger = Logger.getLogger( this.class.name )

    /**
     * API to check if logged in user is part of System or Not. This is used while Sign in
     * Method : POST
     * Request JSON:"{
     'userName':'3080938403840',
     'password':'sjdwljlsj'
     }"
     * @return
     */
    def checkIfUserPresent() {
        def map = request.JSON
        logger.debug( 'checkIfUserPresent map ' + map )
        def userName = map.userName
        def password = map.password
        SecUser savedUser = SecUser.findByUsername( userName )
        def result = [:]
        if (savedUser) {

            def encPass = savedUser.password
            bCryptPasswordEncoder = new BCryptPasswordEncoder()
            Person person = Person.findByPrimaryContact( userName )
            Doctor doctor = Doctor.findByUuid( person.uuid )
            boolean present = bCryptPasswordEncoder.matches( password, encPass )
            if (present) {
                result = [present       : true,
                          name          : "${breathBeatsCommonService.DR_NAME_PREFIX} ${person.firstName} ${person.lastName}",
                          uuid          : person.uuid,
                          qualification : doctor.qualification,
                          doctorImageUrl: DoctorProfile.findByDoctor( doctor )?.doctorImage,
                          speciality    : doctor.speciality?.name]
            } else {
                result = [present: false]
            }
        } else {
            result = [present: false]
        }
        logger.debug( 'checkIfUserPresent result ' + result )
        render result as JSON
    }

    /**
     * API to Sign up new Doctor
     * Method: POST
     * Request JSON:{
     'firstName': 'Shiv',
     'lastName': 'Tiwari',
     'emailAddress':'shiv.tiwari@gmail.com',
     'primaryContact': '89015992293',
     'speciality': 'Ortho',
     'certificateNumber':'BLR78781',
     'practicingCertificatePhoto':'',
     'password': 'password',
     'confirmPassword': 'password'
     }
     * @return
     */
    @Transactional
    def signUpDoctor() {
        def map = request.JSON
        logger.debug( 'signUpDoctor map ' + map )
        def result = [:]
        def validationResult = breathBeatsCommonService.validateRequest( map )
        if (!validationResult.validate) {
            result = validationResult
            result.success = false
            logger.debug( 'signUpDoctor result ' + result )
            render result as JSON
        } else {
            def existingPerson = Person.findByPrimaryContact( map.primaryContact )
            if (existingPerson) {
                def existingDoctorProfile = DoctorProfile.findByDoctor( Doctor.findByUuid( existingPerson.uuid ) )
                if (existingDoctorProfile) {
                    result = [message: 'Your Profile is already present.', success: false]
                    logger.debug( 'signUpDoctor result ' + result )
                    render result as JSON
                    return
                }
            }
            Person person = new Person( createdOn: new Date(),
                                        createdBy: breathBeatsCommonService.CREATED_BY,
                                        uuid: breathBeatsCommonService.getSequenceNumber( 'DOCTOR' ),
                                        firstName: map.firstName,
                                        lastName: map.lastName,
                                        city: map.city,
                                        emailAddress: map.emailAddress,
                                        primaryContact: map.primaryContact,
                                        role: Role.findByRoleKey( 'DOCTOR' )
            )
            if (person.validate()) {
                person.save( flush: true )
                Doctor doctor = new Doctor(
                        uuid: person.uuid,
                        speciality: syncSpeciality( map.speciality ),
                        qualification: map.qualification,
                        certificateNumber: map.certificateNumber,
                        createdOn: new Date(),
                        createdBy: breathBeatsCommonService.CREATED_BY
                )
                if (doctor.validate()) {
                    doctor.save( flush: true )
                    DoctorProfile doctorProfile = new DoctorProfile(
                            doctor: doctor,
                            status: Status.findByName( 'Draft' ),
                            createdOn: new Date(),
                            createdBy: breathBeatsCommonService.CREATED_BY
                    )
                    if (doctorProfile.validate()) {
                        doctorProfile.save( flush: true )
                        SecUser secUser = new SecUser(
                                username: map.primaryContact,
                                firstName: map.firstName,
                                lastName: map.lastName,
                                password: map.password,
                                enabled: true,
                                accountExpired: false,
                                accountLocked: false,
                                passwordExpired: false
                        )
                        if (secUser.validate()) {
                            secUser.save( flush: true )
                            SecUserSecRole secUserSecRole = new SecUserSecRole(
                                    secUser: secUser,
                                    secRole: SecRole.findByAuthority( 'DOCTOR' )
                            )
                            if (secUserSecRole.validate()) {
                                secUserSecRole.save( flush: true )

                                def otpResult = breathBeatsCommonService.sentOtpToMobileTemp( person.primaryContact, person.firstName )
                                if (!otpResult.success) {
                                    if (otpResult.statusCode in [101, 105, 175]) {
                                        result = [message: 'Primary Contact is not valid.' + otpResult.message, success: false]
                                        logger.debug( 'signUpDoctor result ' + result )
                                        render result as JSON
                                    }
                                } else {
                                    AuthTokens authTokens = new AuthTokens(
                                            key: person.uuid,
                                            token: otpResult.otp,
                                            tokenType: 'SMS',
                                            generatedOn: otpResult.generatedDate,
                                            createdBy: breathBeatsCommonService.CREATED_BY,
                                            createdOn: new Date(),
                                            status: 'GENERATED'
                                    )
                                    if (authTokens.validate()) {
                                        authTokens.save( flush: true )
                                    }
                                    result = [uuid   : person.uuid,
                                              otpSent: true,
                                              message: 'Sign UP complete. OTP has been sent to your primary contact. Please use your primary contact as login',
                                              success: true]
                                }
                                logger.debug( 'signUpDoctor result ' + result )
                                render result as JSON
                            } else {
                                result = [message: 'Doctor information is not correct.' + secUserSecRole.errors, success: false]
                                logger.debug( 'signUpDoctor result ' + result )
                                render result as JSON
                            }
                        } else {
                            result = [message: 'Doctor information is not correct.' + secUser.errors, success: false]
                            logger.debug( 'signUpDoctor result ' + result )
                            render result as JSON
                        }

                    } else {
                        result = [message: 'Doctor information is not correct.' + doctorProfile.errors, success: false]
                        logger.debug( 'signUpDoctor result ' + result )
                        render result as JSON
                    }

                } else {
                    result = [message: 'Doctor information is not correct.' + doctor.errors, success: false]
                    logger.debug( 'signUpDoctor result ' + result )
                    render result as JSON
                }

            } else {
                result = [message: 'Doctor information is not correct.' + person.errors, success: false]
                logger.debug( 'signUpDoctor result ' + result )
                render result as JSON
            }
        }


    }

    /**
     * Method to add new Speciality while Sign up
     * @param speciality
     * @return
     */
    private syncSpeciality( speciality ) {
        logger.debug( 'syncSpeciality result ' + speciality )
        Speciality specialityObj = Speciality.findByNameIlike( speciality )
        if (specialityObj) {
            return specialityObj
        } else {
            specialityObj = new Speciality( createdBy: breathBeatsCommonService.CREATED_BY, createdOn: new Date(), name: speciality, description: speciality )
            if (specialityObj.validate()) {
                specialityObj = specialityObj.save( flush: true )
            } else {
                logger.error( 'syncSpeciality  Error while saving Speciality' + specialityObj?.errors )
            }
        }
        specialityObj
    }
}
