package breath.beats.doctor.poc.app

import com.breath.beats.doctor.app.common.BreathBeatsCommonService
import grails.converters.JSON
import grails.transaction.Transactional
import org.apache.log4j.Logger

@Transactional(readOnly = true)
class UserDoctorController {
    Logger logger = Logger.getLogger( this.class.name )
    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]
    def breathBeatsCommonService

    /**
     * API to load patient to doctor notification log , if patient has paid and looking to start to
     * communicate to doctor
     * Method: POST
     * Request JSON:{uuid:'D016100002'}* @return
     */
    def loadPatientNotification() {
        def map = request.JSON
        logger.debug( 'loadPatientNotification map ' + map )
        def hospitalNumber = map.uuid
        List consultations = DoctorPatientConsultation.findAllByDoctorAndConsultationStage( Doctor.findByUuid( hospitalNumber ), ConsultationStage.findByName( 'PAID' ) )
        def result = consultations.collect {DoctorPatientConsultation it ->
            [name          : it.patient.name,
             consStartDate : it.consStartDate,
             mode          : it.consultationMode.name,
             status        : it.consultationStage.name,
             consultationId: it.id
            ]
        }
        if (result.size() > 0) {
            result = result.get( 0 )
        }
        logger.debug( 'loadPatientNotification result ' + result )
        render result as JSON
    }

    /**
     * API to search Doctor based on location and specialities
     * Method: POST
     * Request JSON:{"specialities":[ "Ortho", "General"],
     "location":'Banglore'}* @return
     */
    def searchDoctor() {
        def paramMap = request.JSON
        logger.debug( 'searchDoctor: paramMap' + paramMap )
        def specialities = paramMap.specialities
        String location = paramMap.location
        if (location) {
            location = '%' + location.toUpperCase() + '%'
        } else {
            location = '%'
        }
        List<DoctorDetails> doctorDetails = []
        List arraySpecialities = []
        if (specialities instanceof String) {
            arraySpecialities = specialities.split( "," )
        }
        if (specialities instanceof List) {
            arraySpecialities = paramMap.specialities
        }
        doctorDetails = DoctorDetails.withSession {session ->
            session.createQuery( "from DoctorDetails a where upper(a.city) like :city and a.specialityName in (:specialityNames) " )
                    .setString( "city", location )
                    .setParameterList( "specialityNames", arraySpecialities )
                    .list()

        }
        def result = doctorDetails.collect() {
            DoctorPopularity doctorPopularity = DoctorPopularity.findByDoctor( Doctor.findById( it.doctorId ) )
            def likePercentage = 0
            if (doctorPopularity?.likes) {
                likePercentage = (doctorPopularity.likes * 100) / (doctorPopularity.likes + doctorPopularity.dislike ?: 0)
            }
            [addressLine1          : it.addressLine1,
             addressLine2          : it.addressLine2,
             city                  : it.city,
             name                  : BreathBeatsCommonService.DR_NAME_PREFIX + it.firstName + ' ' + it.lastName,
             firstName             : it.firstName,
             lastName              : it.lastName,
             qualification         : it.qualification,
             specialityName        : it.specialityName,
             primaryContact        : it.primaryContact,
             uuid                  : it.uuid,
             fee                   : it.feeForPatient,
             experience            : it.expYear,
             timeDurationForPatient: it.timeDurationForPatient,
             image                 : it.doctorImageLocUrl,
             likePer               : (likePercentage as int) + '%',
             rating                : 'GOOD']// Need to enhance Rating stuff
        }
        logger.debug( 'searchDoctor result ' + result )
        render result as JSON
    }

    /**
     * Load Doctor profile on user app
     * Method:POST
     * Request JSON:"{""uuid"":'D019200002',}"
     * @return
     */
    def lodDoctorProfileByUuid() {
        def map = request.JSON
        logger.debug( 'lodDoctorProfileByUuid map ' + map )
        def hospitalNumber = map.uuid
        def result = [:]
        Person person = Person.findByUuid( hospitalNumber )
        if (person) {
            result.name = person.firstName + ' ' + person.lastName
            result.uuild = map.uuid
            def doctorProfile = DoctorProfile.findByDoctor( Doctor.findAllByUuid( hospitalNumber ) )
            result.experience = doctorProfile.expYear
            result.feeForPatient = doctorProfile?.feeForPatient
            result.status = doctorProfile?.status.name
        }
        logger.debug( 'lodDoctorProfileByUuid ' + result )
        render result as JSON
    }

    /**
     * API to Send Firebase Message from source user to Destination user
     * Method: POST
     * Request JSON:"{""sentFrom"":'Shiv',
     ""sentTo"":""238268368"",
     ""message"":""Text""}"
     * @return
     */
    def pushFireBaseMessage() {
        def paramMap = request.JSON
        def result = breathBeatsCommonService.pushFireBaseMessage( paramMap.sentFrom, paramMap.sentTo, paramMap.message )
        render result as JSON
    }

    /**
     * API to Book a consultation appointment
     * Method: POST
     * Request JSON" {'contactNumber':'9901599229',
     'patientName':'Sandeep sinha',
     'emailAddress':'sandeep.sinha@gmail.com',
     'mode':'Video',
     'uuid':'D016100002'}               "
     * @return
     */
    @Transactional
    def bookAppointment() {
        def paramMap = request.JSON
        logger.debug( 'bookAppointment: paramMap' + paramMap )
        // Check if Patient is already part of System based on Phone Number
        Patient patient = Patient.findByContactNumber( paramMap.contactNumber )
        def result = [:]
        Doctor doctor = Doctor.findByUuid( paramMap.uuid )
        if (!doctor) {
            logger.debug( 'bookAppointment: Doctor not present' )
            result = [success: false, message: 'Invalid input for Patient Information']
            logger.debug( 'bookAppointment ' + result )
            render result as JSON
            return
        }
        DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
        if (doctorProfile.currentStatus.name == 'On Call') {
            result = [success: false, message: 'Doctor is already on Call']
            logger.debug( 'bookAppointment ' + result )
            render result as JSON
            return
        }
        if (!patient) {
            Patient patient1 = new Patient(
                    name: paramMap.patientName,
                    contactNumber: paramMap.contactNumber,
                    emailAddress: paramMap.emailAddress,
                    createdOn: new Date(),
                    createdBy: 'SYSTEM'
            )
            if (patient1.validate()) {
                patient = patient1.save( flush: true )
            } else {
                logger.debug( 'bookAppointment: patient Save failed' + patient1.errors.getAllErrors() )
                result = [success: false, message: 'Invalid input for Patient Information']
                logger.debug( 'bookAppointment ' + result )
                render result as JSON
                return
            }
        }
        // Find exiting open consultancy
        DoctorPatientConsultation existingDoctorPatientConsultation = DoctorPatientConsultation.findByDoctorAndPatientAndConsultationStageInList(
                Doctor.findByUuid( paramMap.uuid ), patient, ConsultationStage.findAllByNameInList( ['BOOKED', 'PAID', 'ACTIVE', 'IN_PROGRESS'] ) )
        if (existingDoctorPatientConsultation) {
            result = [success: false, message: 'There is existing consultancy present. Please complete the consultation or cancel it']
            logger.debug( 'bookAppointment ' + result )
            render result as JSON
            return
        }
        DoctorPatientConsultation doctorPatientConsultation = new DoctorPatientConsultation(
                doctor: Doctor.findByUuid( paramMap.uuid ),
                patient: patient,
                consultationStage: ConsultationStage.findByName( 'BOOKED' ),
                consStartDate: new Date(),
                createdOn: new Date(),
                consultationMode: ConsultationMode.findByName( paramMap.mode ),
                createdBy: patient.name
        )
        if (doctorPatientConsultation.validate()) {
            doctorPatientConsultation = doctorPatientConsultation.save( flush: true )
            DoctorPatientConsultationLog doctorPatientConsultationLog = new DoctorPatientConsultationLog(
                    doctorPatientConsultation: doctorPatientConsultation,
                    consultationStage: doctorPatientConsultation.consultationStage,
                    createdOn: new Date(),
                    createdBy: patient.name
            )
            if (doctorPatientConsultationLog.validate()) {
                doctorPatientConsultationLog.save( flush: true )
                result = [success: true, message: 'Appointment booked']
            } else {
                logger.debug( 'bookAppointment: doctorPatientConsultation Log Save failed' + doctorPatientConsultationLog.errors.getAllErrors() )
                result = [success: false, message: 'Invalid input for Appointment Log']
                logger.debug( 'bookAppointment ' + result )
                render result as JSON
                return
            }
        } else {
            logger.debug( 'bookAppointment: patient Save failed' + doctorPatientConsultation.errors.getAllErrors() )
            result = [success: false, message: 'Invalid input for Appointment']
            logger.debug( 'bookAppointment ' + result )
            render result as JSON
            return
        }
        logger.debug( 'bookAppointment ' + result )
        render result as JSON
    }

    /**
     * API to update an exiting consultation appointment. This Maintains a life cylce of Consultation
     * Method: POST
     * Request JSON"{""contactNumber"":""9906129229"",
     ""uuid"":""D016900019"",
     ""consultationStage"":""PAID"",
     ""consultationId"":""62a38740-4ec7-11e8-b9b5-061dfc4d1c8c""}"
     * @return
     */
    @Transactional
    def updateAppointment() {
        def paramMap = request.JSON
        logger.debug( 'updateAppointment map ' + paramMap )
        def result = [:]
        Patient patient = Patient.findByContactNumber( paramMap.contactNumber )
        if (!patient) {
            result = [success: false, message: 'Patient not present']
            logger.debug( 'updateAppointment ' + result )
            render result as JSON
            return
        }
        Doctor doctor = Doctor.findByUuid( paramMap.uuid )
        if (!doctor) {
            result = [success: false, message: 'Doctor not present']
            logger.debug( 'updateAppointment ' + result )
            render result as JSON
            return
        }

        // Find exiting open consultancy
        DoctorPatientConsultation existingDoctorPatientConsultation = DoctorPatientConsultation.findById( paramMap.consultationId )
        if (!existingDoctorPatientConsultation) {
            result = [success: false, message: 'There is no existing consultancy present. Please book a  new appointment']
            logger.debug( 'updateAppointment ' + result )
            render result as JSON
            return
        }
        ConsultationStage consultationStage = ConsultationStage.findByName( paramMap.consultationStage )
        if (!consultationStage) {
            result = [success: false, message: 'Incorrect Input for consultation stage']
            logger.debug( 'updateAppointment ' + result )
            render result as JSON
            return
        }
        existingDoctorPatientConsultation.consultationStage = consultationStage
        if (consultationStage.name == 'INACTIVE' || consultationStage.name == 'COMPLETE') {
            existingDoctorPatientConsultation.consEndDate = new Date()
        }
        existingDoctorPatientConsultation.save( flush: true )
        DoctorPatientConsultationLog doctorPatientConsultationLog = new DoctorPatientConsultationLog(
                doctorPatientConsultation: existingDoctorPatientConsultation,
                consultationStage: consultationStage,
                createdOn: new Date(),
                createdBy: patient.name
        )
        if (doctorPatientConsultationLog.validate()) {
            doctorPatientConsultationLog.save( flush: true )
            updateDoctorStatus( existingDoctorPatientConsultation.doctor, ConsultationStage.findByName( paramMap.consultationStage ) )
            [success: true, message: 'Appointment updated']
        } else {
            result = [success: false, message: 'Invalid input for Appointment Log']
            logger.debug( 'updateAppointment ' + result )
            render result as JSON
            return
        }
        logger.debug( 'updateAppointment ' + result )
        render result as JSON
    }

    /**
     * Update doctor status based on how consultation workflow progresses.
     * @param doctor
     * @param consultationStage
     * @return
     */
    private def updateDoctorStatus( Doctor doctor, ConsultationStage consultationStage ) {
        if (doctor && consultationStage) {
            if (consultationStage.name == 'PAID') {
                DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
                doctorProfile.currentStatus = Status.findByName( 'On Call' )
                doctorProfile.createdOn = new Date()
                doctorProfile.save()
            } else if (consultationStage.name in ['COMPLETED', 'INACTIVE', 'READY_TO_REFUND', 'REFUND_COMPLETED', 'IN_PROGRESS']) {
                DoctorProfile doctorProfile = DoctorProfile.findByDoctor( doctor )
                doctorProfile.currentStatus = Status.findByName( 'Available' )
                doctorProfile.createdOn = new Date()
                doctorProfile.save()
            }
        }
    }

    /**
     * AIP to Rate a consultation
     * Method: POST
     * Request JSON:{'consultationId':'d00cc1d5-1b5f-11e8-889f-54887c4c340a',
     'rating':'5',
     'uuid':'D016100002'}* @return
     */
    @Transactional
    def rateDoctor() {
        def paramMap = request.JSON
        logger.debug( 'rateDoctor map ' + paramMap )
        def result = [:]
        Patient patient = Patient.findByContactNumber( paramMap.contactNumber )
        if (!patient) {
            result = [success: false, message: 'Patient not present']
            logger.debug( 'rateDoctor ' + result )
            render result as JSON
            return
        }
        Doctor doctor = Doctor.findByUuid( paramMap.uuid )
        if (!doctor) {
            result = [success: false, message: 'Doctor not present']
            logger.debug( 'rateDoctor ' + result )
            render result as JSON
            return
        }
        // Find exiting open consultancy
        DoctorPatientConsultation existingDoctorPatientConsultation = DoctorPatientConsultation.findById( paramMap.consultationId )
        DoctorRating doctorRating = DoctorRating.findByDoctorPatientConsultation( existingDoctorPatientConsultation )
        if (doctorRating) {
            doctorRating.rating = paramMap.rating
            doctorRating.createdBy = patient.name
            doctorRating.createdOn = new Date()
            doctorRating.save( flush: true )
        } else {
            doctorRating = new DoctorRating(
                    doctor: doctor,
                    doctorPatientConsultation: existingDoctorPatientConsultation,
                    rating: paramMap.rating,
                    createdBy: patient.name,
                    createdOn: new Date()
            )
            doctorRating.save( flush: true )
        }
        result = [success: true, message: 'Rating for apppointment complete']
        logger.debug( 'rateDoctor ' + result )
        render result as JSON
    }

    /**
     * AIP to Like a doctor
     * Method: POST
     * Request JSON:"{uuid:'D016100002',
     'user':'Shiv'}"
     * @return
     */
    @Transactional
    def thumbUpDoctor() {
        def paramMap = request.JSON
        logger.debug( 'thumbUpDoctor map ' + paramMap )
        // Check if Patient is already part of System based on Phone Number
        DoctorProfile doctorProfile = DoctorProfile.findByDoctor( Doctor.findByUuid( paramMap.uuid ) )
        def result = [:]
        if (doctorProfile && doctorProfile.status == Status.findByName( 'Active' )) {
            DoctorPopularity doctorPopularity = DoctorPopularity.findByDoctor( doctorProfile.doctor )
            if (doctorPopularity) {
                doctorPopularity.likes = (doctorPopularity.likes ?: 0) + 1
                doctorPopularity.createdOn = new Date()
                doctorPopularity.createdBy = paramMap.user
                doctorPopularity.save( flush: true )
            } else {
                doctorPopularity = new DoctorPopularity( doctor: doctorProfile.doctor, likes: 1, dislike: 0,
                                                         createdOn: new Date(),
                                                         createdBy: paramMap.user )
                doctorPopularity.save( flush: true )
            }
            result = [success: true, message: 'profile updated']
        } else {
            result = [success: false, message: 'Doctor not present']
        }
        logger.debug( 'thumbUpDoctor ' + result )
        render result as JSON
    }

    /**
     * AIP to dis-Like a doctor
     * Method: POST
     * Request JSON:"{uuid:'D016100002',
     'user':'Shiv'}"
     * @return
     */
    @Transactional
    def thumbDownDoctor() {
        def paramMap = request.JSON
        logger.debug( 'thumbDownDoctor map ' + paramMap )
        // Check if Patient is already part of System based on Phone Number
        DoctorProfile doctorProfile = DoctorProfile.findByDoctor( Doctor.findByUuid( paramMap.uuid ) )
        def result = [:]
        if (doctorProfile && doctorProfile.status == Status.findByName( 'Active' )) {
            DoctorPopularity doctorPopularity = DoctorPopularity.findByDoctor( doctorProfile.doctor )
            if (doctorPopularity) {
                doctorPopularity.dislike = (doctorPopularity.dislike ?: 0) + 1
                doctorPopularity.createdOn = new Date()
                doctorPopularity.createdBy = paramMap.user
                doctorPopularity.save( flush: true )
            } else {
                doctorPopularity = new DoctorPopularity( doctor: doctorProfile.doctor, likes: 0, dislike: 1,
                                                         createdOn: new Date(),
                                                         createdBy: paramMap.user )
                doctorPopularity.save( flush: true )
            }
            result = [success: true, message: 'profile updated']
        } else {
            result = [success: false, message: 'Doctor not present']
        }
        logger.debug( 'thumbDownDoctor ' + result )
        render result as JSON
    }

}
