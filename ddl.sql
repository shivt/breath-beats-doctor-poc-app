DROP DATABASE BREATH_BEATS;
CREATE DATABASE BREATH_BEATS;

USE BREATH_BEATS;

CREATE TABLE BB_SEC_ROLE (
  role_id   VARCHAR(256) PRIMARY KEY,
  authority VARCHAR(256),
  version   INTEGER
);

CREATE TABLE BB_SEC_USER (
  user_id          VARCHAR(256) PRIMARY KEY,
  username         VARCHAR(256),
  first_name       VARCHAR(256),
  last_name        VARCHAR(256),
  password         VARCHAR(256),
  enabled          CHAR(1),
  account_expired  CHAR(1),
  account_locked   CHAR(1),
  password_expired CHAR(1),
  version          INTEGER
);


CREATE TABLE BB_SEC_USER_ROLE (
  sec_user_id VARCHAR(256) NOT NULL,
  sec_role_id VARCHAR(256) NOT NULL,
  version     INTEGER
);


CREATE TABLE bb_general_seq (
  system_id    VARCHAR(30),
  docno_prefix VARCHAR(5),
  system_name  VARCHAR(30),
  seq_maxseqno INTEGER,
  created_on   TIMESTAMP   NOT NULL,
  created_by   VARCHAR(30) NOT NULL,
  version      INTEGER
);

/*table :  role */
CREATE TABLE bb_role (
  role_id          VARCHAR(256) NOT NULL PRIMARY KEY,
  role_key         VARCHAR(256) NOT NULL UNIQUE,
  role_name        VARCHAR(256) NOT NULL,
  role_description VARCHAR(256),
  created_on       TIMESTAMP    NOT NULL,
  created_by       VARCHAR(30)  NOT NULL,
  version          INTEGER
);

/*table :  bb_specialty */
CREATE TABLE bb_speciality (
  speciality_id          VARCHAR(256) NOT NULL PRIMARY KEY,
  speciality_name        VARCHAR(256) NOT NULL UNIQUE,
  speciality_description VARCHAR(300),
  created_on             TIMESTAMP    NOT NULL,
  created_by             VARCHAR(30)  NOT NULL,
  version                INTEGER
);

/*table :  bb_symptoms */
CREATE TABLE bb_symptoms (
  symptoms_id          VARCHAR(256) NOT NULL PRIMARY KEY,
  symptoms_name        VARCHAR(256) NOT NULL UNIQUE,
  symptoms_description VARCHAR(300),
  created_on           TIMESTAMP    NOT NULL,
  created_by           VARCHAR(30)  NOT NULL,
  version              INTEGER
);

/*table :  bb_symptoms_speciality */
CREATE TABLE bb_symptoms_speciality (
  bb_symptoms_speciality VARCHAR(256) NOT NULL PRIMARY KEY,
  symptoms_id            VARCHAR(256) NOT NULL,
  speciality_id          VARCHAR(256) NOT NULL,
  created_on             TIMESTAMP    NOT NULL,
  created_by             VARCHAR(30)  NOT NULL,
  version                INTEGER
);


/*table :  bb_auth_token */
CREATE TABLE bb_auth_token (
  bb_auth_token_id VARCHAR(256) NOT NULL PRIMARY KEY,
  key_id           VARCHAR(256) NOT NULL,
  token            VARCHAR(256) NOT NULL,
  token_type       VARCHAR(256) NOT NULL,
  generated_on     TIMESTAMP    NOT NULL,
  status           VARCHAR(256) NOT NULL,
  last_modified_on TIMESTAMP,
  created_by       VARCHAR(30)  NOT NULL,
  version          INTEGER
);
ALTER TABLE bb_auth_token
  ADD UNIQUE INDEX (key_id, token, status);
/*table :  person */
CREATE TABLE bb_person (
  person_id         VARCHAR(256) NOT NULL PRIMARY KEY,
  hospital_number   VARCHAR(256) NOT NULL UNIQUE,
  role_id           VARCHAR(256) NOT NULL,
  first_name        VARCHAR(256) NOT NULL,
  middle_name       VARCHAR(256),
  last_name         VARCHAR(256),
  gender            CHAR(1),
  email_Address     VARCHAR(256),
  date_of_birth     DATE,
  age               INTEGER(3),
  primary_contact   VARCHAR(20)  NOT NULL,
  secondary_contact VARCHAR(20),
  marital_status    CHAR(1),
  address_line1     VARCHAR(256),
  address_line2     VARCHAR(256),
  address_line3     VARCHAR(256),
  city              VARCHAR(30),
  state_name        VARCHAR(30),
  country           VARCHAR(30),
  zip_code          VARCHAR(30),
  created_on        TIMESTAMP    NOT NULL,
  created_by        VARCHAR(30)  NOT NULL,
  version           INTEGER
);


CREATE UNIQUE INDEX person_uk1
  ON bb_person (hospital_number);
ALTER TABLE bb_person
  ADD FOREIGN KEY person_fk1 (role_id) REFERENCES bb_role (role_id);

/*table :  bb_doctor */
CREATE TABLE bb_doctor (
  doctor_id           VARCHAR(256) NOT NULL PRIMARY KEY,
  hospital_number     VARCHAR(256) NOT NULL UNIQUE,
  speciality_id       VARCHAR(256),
  certificate_number  VARCHAR(256) NOT NULL,
  certificate_img_loc VARCHAR(256),
  qualification       VARCHAR(256),
  created_on          TIMESTAMP    NOT NULL,
  created_by          VARCHAR(30)  NOT NULL,
  version             INTEGER
);

ALTER TABLE bb_doctor
  ADD FOREIGN KEY bb_doctor_fk1 (speciality_id) REFERENCES bb_speciality (speciality_id);


/*table :  bb_doctor_pop */
CREATE TABLE bb_doctor_pop (
  doctor_pop_id VARCHAR(256) NOT NULL PRIMARY KEY,
  doctor_id     VARCHAR(256) NOT NULL,
  likes         INTEGER,
  dislike       INTEGER,
  created_on    TIMESTAMP    NOT NULL,
  created_by    VARCHAR(30)  NOT NULL,
  version       INTEGER
);


ALTER TABLE bb_doctor_pop
  ADD FOREIGN KEY bb_doctor_pop_fk1 (doctor_id) REFERENCES bb_doctor (doctor_id);


/*table :  bb_patient */
CREATE TABLE bb_patient (
  patient_id    VARCHAR(256) NOT NULL PRIMARY KEY,
  name          VARCHAR(256) NOT NULL,
  concat_number VARCHAR(256),
  email_Address VARCHAR(256),
  created_on    TIMESTAMP    NOT NULL,
  created_by    VARCHAR(30)  NOT NULL,
  version       INTEGER
);


CREATE TABLE bb_consultation_stages (
  consultation_stages_id VARCHAR(256) NOT NULL PRIMARY KEY,
  name                   VARCHAR(256) NOT NULL,
  description            VARCHAR(500),
  created_on             TIMESTAMP    NOT NULL,
  created_by             VARCHAR(30)  NOT NULL,
  version                INTEGER
);


CREATE TABLE bb_consultation_mode (
  consultation_mode_id VARCHAR(256) NOT NULL PRIMARY KEY,
  name                 VARCHAR(256) NOT NULL,
  description          VARCHAR(500),
  created_on           TIMESTAMP    NOT NULL,
  created_by           VARCHAR(30)  NOT NULL,
  version              INTEGER
);

CREATE TABLE bb_patient_consultation (
  consultation_id        VARCHAR(256) NOT NULL PRIMARY KEY,
  patient_id             VARCHAR(256) NOT NULL,
  doctor_id              VARCHAR(256) NOT NULL,
  consultation_mode_id   VARCHAR(256), /* Text, Call, Video */
  cons_start_date        TIMESTAMP,
  cons_end_date          TIMESTAMP,
  consultation_stages_id VARCHAR(256) NOT NULL, /* BOOKED, PAID, ACTIVE, IN_PROGRESS, COMPLETED, INACTIVE */
  created_on             TIMESTAMP    NOT NULL,
  created_by             VARCHAR(30)  NOT NULL,
  version                INTEGER
);


ALTER TABLE bb_patient_consultation
  ADD FOREIGN KEY bb_patient_consultation_fk1 (patient_id) REFERENCES bb_patient (patient_id);


ALTER TABLE bb_patient_consultation
  ADD FOREIGN KEY bb_patient_consultation_fk2 (doctor_id) REFERENCES bb_doctor (doctor_id);


ALTER TABLE bb_patient_consultation
  ADD FOREIGN KEY bb_patient_consultation_fk3 (consultation_stages_id) REFERENCES bb_consultation_stages (consultation_stages_id);


CREATE TABLE bb_patient_consultation_log (
  consultation_log_id    VARCHAR(256) NOT NULL PRIMARY KEY,
  consultation_id        VARCHAR(256) NOT NULL,
  consultation_stages_id VARCHAR(256) NOT NULL, /* BOOKED, PAID, ACTIVE, IN_PROGRESS, COMPLETED, INACTIVE */
  created_on             TIMESTAMP    NOT NULL,
  created_by             VARCHAR(30)  NOT NULL,
  version                INTEGER
);


ALTER TABLE bb_patient_consultation_log
  ADD FOREIGN KEY bb_patient_consultation_log_fk1 (consultation_id) REFERENCES bb_patient_consultation (consultation_id);


ALTER TABLE bb_patient_consultation_log
  ADD FOREIGN KEY bb_patient_consultation_log_fk2 (consultation_stages_id) REFERENCES bb_consultation_stages (consultation_stages_id);


/*table :  bb_doctor_pop */
CREATE TABLE bb_doctor_rating (
  doctor_rating_id VARCHAR(256) NOT NULL PRIMARY KEY,
  doctor_id        VARCHAR(256) NOT NULL,
  consultation_id  VARCHAR(256) NOT NULL,
  rating           INTEGER      NOT NULL,
  created_on       TIMESTAMP    NOT NULL,
  created_by       VARCHAR(30)  NOT NULL,
  version          INTEGER
);

ALTER TABLE bb_doctor_rating
  ADD FOREIGN KEY bb_doctor_rating_fk1 (doctor_id) REFERENCES bb_doctor (doctor_id);


ALTER TABLE bb_doctor_rating
  ADD FOREIGN KEY bb_doctor_rating_fk2 (consultation_id) REFERENCES bb_patient_consultation (consultation_id);


/*table :  bb_hospital */
CREATE TABLE bb_hospital (
  hospital_id       VARCHAR(256) NOT NULL PRIMARY KEY,
  hospital_name     VARCHAR(256),
  hospital_location VARCHAR(256),
  created_on        TIMESTAMP    NOT NULL,
  created_by        VARCHAR(30)  NOT NULL,
  version           INTEGER
);

/*table :  bb_status */
CREATE TABLE bb_status (
  status_id   VARCHAR(256) NOT NULL PRIMARY KEY,
  status_name VARCHAR(256) NOT NULL UNIQUE,
  status_desc VARCHAR(256),
  created_on  TIMESTAMP    NOT NULL,
  created_by  VARCHAR(30)  NOT NULL,
  version     INTEGER
);


/*table :  bb_fcm_entity */
CREATE TABLE bb_fcm_entity (
  fcm_entity_id       VARCHAR(256) NOT NULL PRIMARY KEY,
  device_phone_number VARCHAR(256),
  fcm_number           VARCHAR(256),
  created_on          TIMESTAMP    NOT NULL,
  created_by          VARCHAR(30)  NOT NULL,
  version             INTEGER
);


/*table :  bb_doctor_profile */
CREATE TABLE bb_doctor_profile (
  doctor_profile_id  VARCHAR(256) NOT NULL PRIMARY KEY,
  doctor_id          VARCHAR(256),
  fee_for_patient    INTEGER,
  fee_for_discussion INTEGER,
  doctor_image       VARCHAR(256),
  status_id          VARCHAR(256),
  created_on         TIMESTAMP    NOT NULL,
  created_by         VARCHAR(30)  NOT NULL,
  version            INTEGER
);
ALTER TABLE bb_doctor_profile
  ADD FOREIGN KEY bb_doctor_profile_fk1 (doctor_id) REFERENCES bb_doctor (doctor_id);
ALTER TABLE bb_doctor_profile
  ADD FOREIGN KEY bb_doctor_profile_fk2 (status_id) REFERENCES bb_status (status_id);

ALTER TABLE bb_doctor_profile
  ADD currentStatus_id VARCHAR(256) NULL;

ALTER TABLE bb_doctor_profile
  ADD blood_group VARCHAR(10) NULL;

ALTER TABLE bb_doctor_profile
  ADD exp_year INTEGER NULL;

ALTER TABLE bb_doctor_profile
  ADD hospital_worked_in VARCHAR(1000);

ALTER TABLE bb_doctor_profile
  ADD certifications VARCHAR(1000);

ALTER TABLE bb_doctor_profile
  ADD degrees_held VARCHAR(1000);

ALTER TABLE bb_doctor_profile
  ADD awards VARCHAR(1000);

ALTER TABLE bb_doctor_profile
  ADD home_care_availability VARCHAR(1);

ALTER TABLE bb_doctor_profile
  ADD clinic_contant_number VARCHAR(10);

ALTER TABLE bb_doctor_profile
  ADD clinic_address VARCHAR(1000);

ALTER TABLE bb_doctor_profile
  ADD field_1 VARCHAR(50);


ALTER TABLE bb_doctor_profile
  ADD field_2 VARCHAR(50);


ALTER TABLE bb_doctor_profile
  ADD field_3 VARCHAR(50);

ALTER TABLE bb_doctor_profile
  ADD field_4 VARCHAR(50);

ALTER TABLE bb_doctor_profile
  ADD field_5 VARCHAR(50);

ALTER TABLE bb_doctor_profile
  ADD time_duration_for_patient INTEGER;


ALTER TABLE bb_doctor_profile
  ADD time_duration_for_discussion INTEGER;


ALTER TABLE bb_doctor_profile
  ADD FOREIGN KEY bb_doctor_profile_fk3 (currentStatus_id) REFERENCES bb_status (status_id);

/*  DMLS */

INSERT INTO bb_role VALUES
  ('1000000000000000001', 'PATIENT', 'Patient', 'Patient visiting hospital, they can be OPD or impatient',
   current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_role VALUES
  ('1000000000000000002', 'DOCTOR', 'Doctor', 'Doctors visiting hospital or employee of hospital', current_timestamp,
   'SYSTEM', 0);

INSERT INTO bb_general_seq VALUES ('DOCTOR', 'D', 'DOCTOR', 1, current_timestamp, 'SYSTEM', 0);

INSERT INTO bb_hospital
VALUES ('1000000000000000001', 'Manipal', 'Old Airport Road, Bangalore', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_hospital
VALUES ('1000000000000000002', 'Narayana HSR', 'HSR, Bangalore', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_hospital
VALUES ('1000000000000000003', 'Appolo Bangalore South', 'Banarkatta Road, Bangalore', current_timestamp, 'SYSTEM', 0);

INSERT INTO bb_status VALUES ('1000000000000000001', 'On Call', 'On Call', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_status VALUES ('1000000000000000002', 'On Duty', 'On Duty', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_status VALUES ('1000000000000000003', 'Available', 'Available', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_status VALUES ('1000000000000000004', 'Not Available', 'Not Available', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_status
VALUES ('1000000000000000005', 'Draft', 'Profile pending for Approved', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_status VALUES ('1000000000000000006', 'Approved', 'Profile Approved', current_timestamp, 'SYSTEM', 0);
INSERT INTO bb_status
VALUES ('1000000000000000007', 'Rejected', 'Profile Not Approved', current_timestamp, 'SYSTEM', 0);

INSERT INTO bb_speciality VALUES ('1000000000000000001', 'Ortho', 'Ortho', current_timestamp, 'SYSTEM', 0);

INSERT INTO bb_speciality VALUES ('1000000000000000002', 'Radiology', 'Radiology', current_timestamp, 'SYSTEM', 0);

INSERT INTO bb_speciality VALUES ('1000000000000000003', 'General', 'General', current_timestamp, 'SYSTEM', 0);


INSERT INTO BB_SEC_USER VALUES
  (999999, 'shiv.tiwari@gmail.com', 'Shiv', 'Tiwari', '$2a$10$Wk4hWPO4M84M2yWDIkJ8O.wyqr./0QmhkkZ0WvBVLD5uzptNdeVOa', 1,
   0, 0, 0, 0);

INSERT INTO BB_SEC_ROLE VALUES ('1000000000000000001', 'PATIENT', 0);
INSERT INTO BB_SEC_ROLE VALUES ('1000000000000000002', 'DOCTOR', 0);
INSERT INTO BB_SEC_ROLE VALUES ('1000000000000000003', 'STAFF', 0);


INSERT INTO BB_SEC_USER_ROLE VALUES ('1000000000000000001', '1000000000000000002', 0);

#BOOKED -> READY_TO_PAY->PAID->ACTIVE->IN_PROGRESS->COMPLETED
#BOOKED -> READY_TO_PAY->PAID->INACTIVE->READY_TO_REFUND->REFUND_COMPLETED
#BOOKED -> READY_TO_PAY->PAID->READY_TO_REFUND->REFUND_COMPLETED
#BOOKED -> INACTIVE
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000001', 'BOOKED', 'Booked Consultation', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000002', 'PAID', 'Fee Paid', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages VALUES
  ('1000000000000000003', 'ACTIVE', 'Consultation is active and available to doctor for action', current_timestamp,
   'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000004', 'IN_PROGRESS', 'Consultation in Progress', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000005', 'COMPLETED', 'Consultation Completed', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000006', 'INACTIVE', 'Consultation inactive', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000007', 'READY_TO_REFUND', 'Ready To Refund', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000008', 'REFUND_COMPLETED', 'Refund Complete inactive', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_stages
VALUES ('1000000000000000009', 'NOTIFIED', 'Refund Complete inactive', current_timestamp, 'INITIAL_DATA', 0);

INSERT INTO bb_consultation_mode
VALUES ('1000000000000000001', 'Text', 'Text Mode, Doctor and Patient can chat', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_mode
VALUES ('1000000000000000002', 'Call', 'Call Mode, Patien can call to doctor', current_timestamp, 'INITIAL_DATA', 0);
INSERT INTO bb_consultation_mode VALUES
  ('1000000000000000003', 'Video', 'Doctor and Patient can join a Video confrence', current_timestamp, 'INITIAL_DATA',
   0);

UPDATE bb_status
SET status_name = 'Draft', status_desc = 'Profile pending for Approved'
WHERE status_id = '1000000000000000005';
UPDATE bb_status
SET status_name = 'Approved', status_desc = 'Profile Approved'
WHERE status_id = '1000000000000000006';
INSERT INTO bb_status
VALUES ('1000000000000000007', 'Rejected', 'Profile Not Approved', current_timestamp, 'SYSTEM', 0);
CREATE OR REPLACE VIEW bb_v_doctor_details AS
  SELECT
        doc.qualification,
        doc.speciality_id,
        doc.hospital_number,
        spec.speciality_name,
        person.address_line1,
        person.address_line2,
        person.first_name,
        person.last_name,
        person.email_Address,
        person.primary_contact,
        person.city,
        person.zip_code,
        doc.doctor_id,
        person.person_id,
        prof.doctor_image,
        status.status_name,
        prof.exp_year,
        prof.fee_for_patient,
        prof.time_duration_for_patient,
        currnt_status.status_name current_status
      FROM bb_doctor doc
        JOIN bb_speciality spec ON doc.speciality_id = spec.speciality_id
        JOIN bb_person person
          ON person.hospital_number = doc.hospital_number
        JOIN bb_doctor_profile prof ON prof.doctor_id = doc.doctor_id
        JOIN bb_status status
        ON status.status_id=prof.status_id
         JOIN bb_status currnt_status
        ON currnt_status.status_id=prof.currentStatus_id
    WHERE prof.status_id ='1000000000000000006'
    and coalesce(prof.currentStatus_id,'1000000000000000003')='1000000000000000003';

DELIMITER $$
CREATE DEFINER =`breathbeats`@`localhost` PROCEDURE `bb_gen_seq_number`(IN  system_id VARCHAR(30),
                                                                        OUT new_seq   VARCHAR(30))
  BEGIN
    SET SQL_SAFE_UPDATES = 0;
    UPDATE bb_general_seq
    SET seq_maxseqno = seq_maxseqno + 1
    WHERE system_id = system_id;
    SELECT concat(docno_prefix, concat(lpad(FLOOR(RAND() * 500), 4, 0), lpad(seq_maxseqno, 5, 0)))
    INTO new_seq
    FROM bb_general_seq
    WHERE system_id = system_id;
  END$$


