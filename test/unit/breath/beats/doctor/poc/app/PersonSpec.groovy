package breath.beats.doctor.poc.app

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.domain.DomainClassUnitTestMixin} for usage instructions
 */
@TestFor(Person)
class PersonSpec extends Specification {

    def setup() {
    }

    def cleanup() {
    }

    void "test something"() {
        Random rand = new Random();
        for (int i=0;i<100;i++) {
            int pickedNumber = rand.nextInt( Long.MAX_VALUE ) + 1;
            println( pickedNumber )
        }
    }
}
